//
//  IInputHome.swift
//  Viper
//
//  Created by Lucy on 7/29/16.
//  Copyright © 2016 cy. All rights reserved.
//

import Foundation

protocol IInputHome: NSObjectProtocol {
  func doGotoImageEditor() 
}