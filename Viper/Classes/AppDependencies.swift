//
//  AppDependencies.swift
//  Viper
//
//  Created by Lucy on 6/22/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

class AppDependencies {
  var wfLogin: WfLogin = WfLogin ()
  
  init() {
    self.configureDependencies()
  }
  
  func installRootVCIntoWindow(window: UIWindow) {
    self.wfLogin.anchorAsRoot(window)
  }
  
  func configureDependencies() {
    
    let pLogin: PresenterLogin = PresenterLogin()
    //let dmLogin: DmLogin = DmLogin()
    let iLogin:ILogin = ILogin()
   
    let wfHome: WfHome = WfHome()
    let pHome: PresenterHome = PresenterHome()
    let iHome: IHome = IHome()
  
    let wfRoot: WfRoot = WfRoot()
    
    let wfImageEditor: WfImageEditor = WfImageEditor()
    
    pLogin.wfLogin = self.wfLogin
    pLogin.iLogin = iLogin
    pLogin.iLogin?.iOutputLogin = pLogin
    pLogin.iInputLogin = iLogin
    
    self.wfLogin.presLogin = pLogin
    self.wfLogin.wfRoot = wfRoot
    self.wfLogin.wfHome = wfHome
    
    pHome.wfHome = wfHome
    pHome.iHome = iHome
    pHome.iHome?.iOuputHome = pHome
    pHome.iInputHome = iHome
   
    wfHome.wfRoot = wfRoot
    wfHome.presHome = pHome
    wfHome.wfImageEditor = wfImageEditor
  }
}
