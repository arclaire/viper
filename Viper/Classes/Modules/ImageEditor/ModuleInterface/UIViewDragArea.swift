//
//  UIViewDragArea.swift
//  Viper
//
//  Created by Lucy on 8/12/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

protocol DelegateViewDragArea: NSObjectProtocol {
  func delegateDragViewArea(senderPanGesture: UIPanGestureRecognizer)
}

class UIViewDragArea: UIView {

  var panGesture: UIPanGestureRecognizer!
  weak var delviewDragArea: DelegateViewDragArea?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.doDragArea(_:)))
    self.addGestureRecognizer(self.panGesture)
    self.backgroundColor = UIColor(white: 0, alpha: 0.5)
    self.layer.borderColor = COLOR_SILVER.CGColor
    self.layer.borderWidth = 1.0
  
  }
  
  func doDragArea(panGesture: UIPanGestureRecognizer) {
    self.delviewDragArea?.delegateDragViewArea(panGesture)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
