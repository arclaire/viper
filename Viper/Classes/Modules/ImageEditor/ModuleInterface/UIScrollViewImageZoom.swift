//
//  UIScrollViewImageZoom.swift
//  Viper
//  Created by Nguyen Cong Huy on 1/19/16.
//  Modified by Lucy on 8/2/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

let COLOR_BACKGROUND = UIColor.whiteColor()
class UIScrollViewImageZoom: UIScrollView {

  static let kZoomInFactorFromMinWhenDoubleTap: CGFloat = 2
  
  var imgVw: UIImageViewWithMask? = nil
  var imageSize: CGSize = CGSizeZero
  private var pointToCenterAfterResize: CGPoint = CGPointZero
  private var scaleToRestoreAfterResize: CGFloat = 1.0
    
  var maxScaleFromMinScale: CGFloat = 3.0
  
  override var frame: CGRect {
    willSet {
      if CGRectEqualToRect(frame, newValue) == false && CGRectEqualToRect(newValue, CGRectZero) == false && CGSizeEqualToSize(self.imageSize, CGSizeZero) == false {
        self.prepareToResize()
      }
    }
    
    didSet {
      if CGRectEqualToRect(frame, oldValue) == false && CGRectEqualToRect(frame, CGRectZero) == false && CGSizeEqualToSize(self.imageSize, CGSizeZero) == false {
        self.recoverFromResizing()
      }
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.prepareUI()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    self.prepareUI()
  }
  
  deinit {
    print("UIScrollviewImageZoom  deallocated")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
  }
  
  private func prepareUI() {
    showsVerticalScrollIndicator = false
    showsHorizontalScrollIndicator = false
    bouncesZoom = true
    self.alwaysBounceVertical = true
    self.alwaysBounceHorizontal = true
    self.backgroundColor = COLOR_BACKGROUND
    self.imgVw?.backgroundColor = COLOR_BACKGROUND
    decelerationRate = UIScrollViewDecelerationRateFast
    delegate = self
  }
  
  func adjustFrameToCenter() {
    
    guard self.imgVw != nil else {
      return
    }
    
    var frameToCenter = self.imgVw!.frame
    
    // center horizontally
    if frameToCenter.size.width < bounds.width {
      frameToCenter.origin.x = (self.bounds.width - frameToCenter.size.width) / 2
    } else {
      frameToCenter.origin.x = 0
    }
    
    // center vertically
    if frameToCenter.size.height < bounds.height {
      frameToCenter.origin.y = (self.bounds.height - frameToCenter.size.height) / 2
    } else {
      frameToCenter.origin.y = 0
    }
    
    self.imgVw!.frame = frameToCenter
  }
  
  private func prepareToResize() {
    let boundsCenter = CGPoint(x: CGRectGetMidX(bounds), y: CGRectGetMidY(bounds))
    self.pointToCenterAfterResize = convertPoint(boundsCenter, toView: self.imgVw)
    
    self.scaleToRestoreAfterResize = zoomScale
    
    // If we're at the minimum zoom scale, preserve that by returning 0, which will be converted to the minimum
    // allowable scale when the scale is restored.
    if self.scaleToRestoreAfterResize <= minimumZoomScale + CGFloat(FLT_EPSILON) {
      scaleToRestoreAfterResize = 0
    }
  }
  
  private func recoverFromResizing(){
    
    self.setMaxMinZoomScalesForCurrentBounds()
    
    // restore zoom scale, first making sure it is within the allowable range.
    let maxZoomScale = max(minimumZoomScale, self.scaleToRestoreAfterResize)
    zoomScale = min(maximumZoomScale, maxZoomScale)
    
    // restore center point, first making sure it is within the allowable range.
    
    // convert our desired center point back to our own coordinate space
    let boundsCenter = convertPoint(self.pointToCenterAfterResize, toView: self.imgVw)
    
    // calculate the content offset that would yield that center point
    var offset = CGPoint(x: boundsCenter.x - self.bounds.size.width/2.0, y: boundsCenter.y - self.bounds.size.height/2.0)
    
    // restore offset, adjusted to be within the allowable range
    let maxOffset = maximumContentOffset()
    let minOffset = minimumContentOffset()
    
    var realMaxOffset = min(maxOffset.x, offset.x)
    offset.x = max(minOffset.x, realMaxOffset)
    
    realMaxOffset = min(maxOffset.y, offset.y)
    offset.y = max(minOffset.y, realMaxOffset)
    
    contentOffset = offset
  }
  
  private func maximumContentOffset() -> CGPoint {
    return CGPointMake(self.contentSize.width - self.bounds.width, self.contentSize.height - self.bounds.height)
  }
  
  private func minimumContentOffset() -> CGPoint {
    return CGPointZero
  }

  // MARK: - Display image
  
  func displayImage(image: UIImage) {
    self.layoutIfNeeded()
    
    if let _: UIImageView = self.imgVw {
      self.imgVw!.removeFromSuperview()
       debugPrint("remove")
    }
    
    self.imgVw = UIImageViewWithMask(image: image)
    self.imgVw?.backgroundColor = COLOR_BACKGROUND
    self.imgVw?.contentMode = .ScaleAspectFit
    self.imgVw!.userInteractionEnabled = true
    addSubview(self.imgVw!)

    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UIScrollViewImageZoom.doubleTapGestureRecognizer(_:)))
    tapGesture.numberOfTapsRequired = 2
    self.imgVw!.addGestureRecognizer(tapGesture)
    //debugPrint("Size", image.size,"Image_view", self.imgVw)
    self.configureImageForSize(image.size)
  }

  private func configureImageForSize(size: CGSize) {
    self.imageSize = size
    self.contentSize = self.imageSize
    //debugPrint("Size", self.imageSize,"Image_view", self.contentSize)
    self.setMaxMinZoomScalesForCurrentBounds()
    self.zoomScale = minimumZoomScale
    self.contentOffset = CGPointZero
  }
  
  private func setMaxMinZoomScalesForCurrentBounds() {
    // calculate min/max zoomscale
    let xScale = self.bounds.width / self.imageSize.width    // the scale needed to perfectly fit the image width-wise
    let yScale = self.bounds.height / self.imageSize.height   // the scale needed to perfectly fit the image height-wise
    //debugPrint("WIDDDDTH", self.bounds.width)
    // fill width if the image and phone are both portrait or both landscape; otherwise take smaller scale
    let imagePortrait = self.imageSize.height > self.imageSize.width
    let phonePortrait = bounds.height >= bounds.width
    var minScale = (imagePortrait == phonePortrait) ? xScale : min(xScale, yScale)
    
    let maxScale = self.maxScaleFromMinScale * minScale
    //debugPrint("maxscale", maxScale)
    
    // don't let minScale exceed maxScale. (If the image is smaller than the screen, we don't want to force it to be zoomed.)
    if minScale > maxScale {
      minScale = maxScale
    }
    
    self.maximumZoomScale = maxScale
    self.minimumZoomScale = minScale * 0.999 // the multiply factor to prevent user cannot scroll page while they use this control in UIPageViewController
  }
  
  // MARK: - Gesture
  
  @objc private func doubleTapGestureRecognizer(gestureRecognizer: UIGestureRecognizer) {
    // zoom out if it bigger than middle scale point. Else, zoom in
    if zoomScale >= maximumZoomScale / 2.0 {
      setZoomScale(minimumZoomScale, animated: true)
    }
    else {
      let center = gestureRecognizer.locationInView(gestureRecognizer.view)
      let zoomRect = zoomRectForScale(UIScrollViewImageZoom.kZoomInFactorFromMinWhenDoubleTap * minimumZoomScale, center: center)
      zoomToRect(zoomRect, animated: true)
    }
  }
  
  private func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
    var zoomRect = CGRectZero
    
    // the zoom rect is in the content view's coordinates.
    // at a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    // as the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = frame.size.height / scale
    zoomRect.size.width  = frame.size.width / scale
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x = center.x - (zoomRect.size.width / 2.0)
    zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0)
    
    
    return zoomRect
  }
  
  func refresh() {
    if let image = self.imgVw?.image {
      self.displayImage(image)
    }
  }
  
}

extension UIScrollViewImageZoom: UIScrollViewDelegate {
  
  // MARK: - UIScrollViewDelegate
  func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
    return self.imgVw
  }
  
  func scrollViewDidZoom(scrollView: UIScrollView) {
    self.adjustFrameToCenter()
    //debugPrint("CURRENTSCALE", self.zoomScale, "CURRENT SIZE", self.imgVw?.image?.size)
  }
  
}
