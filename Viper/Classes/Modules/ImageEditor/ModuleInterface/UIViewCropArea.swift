//
//  UIViewCropArea.swift
//  Viper
//
//  Created by Lucy on 8/4/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

protocol DelegateUIViewCropArea:NSObjectProtocol {
  func panGestureFromUIViewCropArea(panGesture: UIGestureRecognizer)
}

let COLOR_SILVER = UIColor(red: 219.0/255.0, green: 219.0/255.0, blue: 219.0/255.0, alpha: 1.0)
let COLOR_OPTION_BG = UIColor(red: 250.0/255.0, green: 250.0/255.0, blue: 250.0/255.0, alpha: 1.0)
let COLOR_OPTION_TEXT = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
let COLOR_BORDER = UIColor.clearColor()


class UIViewCropArea: UIView {

  //MARK: Attributes
  private var fillColor: UIColor = UIColor.clearColor()
  private var lineWidth: CGFloat = 2.0
  
  var pathCircle: UIBezierPath = UIBezierPath()
  var pathSquare: UIBezierPath = UIBezierPath()
  let shapeCircle: CAShapeLayer = CAShapeLayer()
  let shapeSquare: CAShapeLayer = CAShapeLayer()
  
  lazy var vwScaleTopLeft: UIViewDragArea = UIViewDragArea()
  lazy var vwScaleTopRight: UIViewDragArea = UIViewDragArea()
  lazy var vwScaleBotLeft: UIViewDragArea = UIViewDragArea()
  lazy var vwScaleBotRight: UIViewDragArea = UIViewDragArea()
  let floatDefaultScaleSize: CGSize = CGSizeMake(20, 20)

  var vwReceiver: UIView? {
    didSet {
      self.vwDummy = UIView(frame: (self.vwReceiver?.frame)!)
      self.vwDummy?.backgroundColor = UIColor.clearColor()
      self.vwDummy?.removeFromSuperview()
      self.addSubview(self.vwDummy!)
      self.vwDummy!.addGestureRecognizer(self.panGeture)
    }
  }
  var vwDummy: UIView?
  
  private let overlaySizeMin:CGSize = CGSizeMake(80, 80)
  
  private var overlaySizeDefault: CGSize = CGSizeMake(270, 270)
  
  private var panGeture: UIPanGestureRecognizer!
  private var pinchGesture: UIPinchGestureRecognizer!
  
  var overlaySize: CGSize = CGSizeMake(270, 270)
  var position: CGPoint = CGPointZero
  var isACircle: Bool = false
  
  weak var delUIViewCropArea: DelegateUIViewCropArea?
  
  // MARK: - Lifecycle
  
  init(origin: CGPoint, width: CGFloat,   height: CGFloat) {
    super.init(frame: CGRectMake(origin.x, origin.y, width, height))
    var floatSquareSize: CGFloat = width
    
    if floatSquareSize > height {
      floatSquareSize = height
    }
    
    self.overlaySizeDefault = CGSizeMake(floatSquareSize , floatSquareSize)
   
    self.overlaySize = CGSizeMake(self.overlaySizeDefault.width-30, self.overlaySizeDefault.height-30)
    
    self.backgroundColor = self.fillColor
    
    self.panGeture = UIPanGestureRecognizer(target: self, action: #selector(UIViewCropArea.panning(_:)))
    if (self.vwDummy != nil) {
      self.vwDummy!.addGestureRecognizer(self.panGeture)
    }
    
    self.pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(UIViewCropArea.pinching(_:)))
    self.addGestureRecognizer(self.pinchGesture)
    
    let overlayFrame: CGRect = CGRectMake((frame.width - self.overlaySize.width) / 2, (frame.height - self.overlaySize.height)/2, self.overlaySize.width, self.overlaySize.height)
    
    self.position = CGPointMake(overlayFrame.origin.x, overlayFrame.origin.y)
    
    
  }    
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  deinit {
    print("UIViewCropArea deallocated")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    //debugPrint("layoutsubviews crop")
    
  }
  
  //MARK: - UI Methods
  
  func addLayerCircleMask() {
    //Do onetime setup of the shape layer.
    self.isACircle = true
    self.shapeCircle.opaque = false
    self.shapeCircle.fillColor = UIColor(white: 0, alpha: 0.8).CGColor
    self.shapeCircle.fillRule = kCAFillRuleEvenOdd
    
    self.shapeCircle.borderWidth = 2.0
    
    self.shapeCircle.strokeColor = COLOR_SILVER.CGColor
    
    self.shapeCircle.lineDashPattern = [1,4]
    
    self.shapeCircle.lineDashPattern = [4,8] // size width, distance
    
    self.shapeCircle.borderWidth = 1.0
    self.layer.addSublayer(self.shapeCircle)
  }
  
  func addLayerSquareMask() {
    
    self.isACircle = false
    
    self.shapeSquare.opaque = false
    self.shapeSquare.fillColor = UIColor(white: 0, alpha: 0.8).CGColor
    self.shapeSquare.fillRule = kCAFillRuleEvenOdd
    
    self.shapeSquare.borderWidth = 2.0
    
    self.shapeSquare.strokeColor = COLOR_SILVER.CGColor
    
    self.shapeSquare.lineDashPattern = [4,8] // size width, distance
    
    self.shapeSquare.borderWidth = 1.0
    self.layer.addSublayer(self.shapeSquare)
    
  }
  
  func addScalingView(vwParent vwParent: UIView) {
    
    guard self.vwReceiver != nil else {
      return
    }
    
    var rect: CGRect = CGRectZero
    
    rect.origin.x = self.position.x + (self.vwReceiver?.frame.origin.x)! - (self.floatDefaultScaleSize.width / 2)
    rect.origin.y = self.position.y + (self.vwReceiver?.frame.origin.y)! - (self.floatDefaultScaleSize.height / 2)
    rect.size = self.floatDefaultScaleSize
    
    //debugPrint("FRAME", rect)
    
    self.vwScaleTopLeft = UIViewDragArea(frame: rect)
    self.vwScaleTopLeft.delviewDragArea = self
    
    rect.origin.x = self.position.x + (self.vwReceiver?.frame.origin.x)! + self.overlaySize.width - (self.floatDefaultScaleSize.width / 2)
    rect.origin.y = self.position.y + (self.vwReceiver?.frame.origin.y)! - (self.floatDefaultScaleSize.height / 2)
    self.vwScaleTopRight = UIViewDragArea(frame: rect)
    self.vwScaleTopRight.delviewDragArea = self
    
    rect.origin.x = self.position.x + (self.vwReceiver?.frame.origin.x)! - (self.floatDefaultScaleSize.width / 2)
    rect.origin.y = self.position.y + (self.vwReceiver?.frame.origin.y)! + self.overlaySize.height - self.floatDefaultScaleSize.height + (self.floatDefaultScaleSize.height / 2)
    self.vwScaleBotLeft = UIViewDragArea(frame: rect)
    self.vwScaleBotLeft.delviewDragArea = self
    
    rect.origin.x = self.position.x + (self.vwReceiver?.frame.origin.x)! + self.overlaySize.width - self.floatDefaultScaleSize.width + (self.floatDefaultScaleSize.width / 2)
    rect.origin.y = self.position.y + (self.vwReceiver?.frame.origin.y)! + self.overlaySize.height - self.floatDefaultScaleSize.height  + (self.floatDefaultScaleSize.height / 2)
    self.vwScaleBotRight = UIViewDragArea(frame: rect)
    self.vwScaleBotRight.delviewDragArea = self
    
    vwParent.addSubview(self.vwScaleTopLeft)
    vwParent.addSubview(self.vwScaleTopRight)
    vwParent.addSubview(self.vwScaleBotLeft)
    vwParent.addSubview(self.vwScaleBotRight)
  }
  
  func removeScalingView() {
    self.vwScaleBotLeft.removeFromSuperview()
    self.vwScaleTopRight.removeFromSuperview()
    self.vwScaleTopLeft.removeFromSuperview()
    self.vwScaleBotRight.removeFromSuperview()
  }

  func repositionViewScale() {
    guard self.vwReceiver != nil else {
      return
    }
    
    var rect: CGRect = CGRectZero
    
    rect.origin.x = self.position.x + (self.vwReceiver?.frame.origin.x)! - (self.floatDefaultScaleSize.width / 2)
    rect.origin.y = self.position.y + (self.vwReceiver?.frame.origin.y)! - (self.floatDefaultScaleSize.height / 2)
    rect.size = self.floatDefaultScaleSize
    
    //debugPrint("FRAME", rect)
    
    self.vwScaleTopLeft.frame = rect
    
    rect.origin.x = self.position.x + (self.vwReceiver?.frame.origin.x)! + self.overlaySize.width - self.floatDefaultScaleSize.width + (self.floatDefaultScaleSize.width / 2)
    rect.origin.y = self.position.y + (self.vwReceiver?.frame.origin.y)! - (self.floatDefaultScaleSize.height / 2)
    self.vwScaleTopRight.frame = rect
    
    rect.origin.x = self.position.x + (self.vwReceiver?.frame.origin.x)! - (self.floatDefaultScaleSize.width / 2)
    rect.origin.y = self.position.y + (self.vwReceiver?.frame.origin.y)! + self.overlaySize.height - self.floatDefaultScaleSize.height + (self.floatDefaultScaleSize.height / 2)
    self.vwScaleBotLeft.frame = rect
    
    rect.origin.x = self.position.x + (self.vwReceiver?.frame.origin.x)! + self.overlaySize.width - self.floatDefaultScaleSize.width + (self.floatDefaultScaleSize.width / 2)
    rect.origin.y = self.position.y + (self.vwReceiver?.frame.origin.y)! + self.overlaySize.height - self.floatDefaultScaleSize.height + (self.floatDefaultScaleSize.height / 2)
    self.vwScaleBotRight.frame = rect
  }

  func bringScaleViewToFront() {
    self.superview!.bringSubviewToFront(self.vwScaleBotRight)
    self.superview!.bringSubviewToFront(self.vwScaleBotLeft)
    self.superview!.bringSubviewToFront(self.vwScaleTopLeft)
    self.superview!.bringSubviewToFront(self.vwScaleTopRight)

  }
  
  func reDrawMasking(rect overlayFrame:CGRect) {
    
    self.position = CGPointMake(overlayFrame.origin.x, overlayFrame.origin.y)
    self.vwDummy?.frame.size = (self.overlaySize)
    self.vwDummy?.frame.origin.x = overlayFrame.origin.x
    self.vwDummy?.frame.origin.y = overlayFrame.origin.y
    
    if self.isACircle {
      
      self.pathCircle = UIBezierPath(ovalInRect: overlayFrame)
      
      self.pathCircle.usesEvenOddFillRule = true
      
      let pathRounded: UIBezierPath = UIBezierPath(roundedRect: self.vwReceiver!.bounds, cornerRadius: 0)
      pathRounded.appendPath(self.pathCircle)
      pathRounded.usesEvenOddFillRule = true
      self.shapeCircle.path = pathRounded.CGPath
      
    } else {
      
      self.pathSquare = UIBezierPath(roundedRect:overlayFrame, cornerRadius: 0)
      
      self.pathSquare.usesEvenOddFillRule = true
      
      let pathRounded: UIBezierPath = UIBezierPath(roundedRect: self.vwReceiver!.bounds, cornerRadius: 0)
      pathRounded.appendPath(self.pathSquare)
      pathRounded.usesEvenOddFillRule = true
      self.shapeSquare.path = pathRounded.CGPath
    }
  }
  
  //MARK: - Action Methods
  
  func panning(panGesture: UIPanGestureRecognizer) {
    self.superview!.bringSubviewToFront(self)
    self.reMove(panGesture)
    self.bringScaleViewToFront()
  }
  
  func pinching(panGesture: UIPinchGestureRecognizer) {
    self.superview!.bringSubviewToFront(self)
    let scale = panGesture.scale
    //debugPrint("PINCH SCALEE", scale)
    self.reScalePath(scale: scale, panGesture: panGesture)
    self.bringScaleViewToFront()
  }
  
  override func drawRect(rect: CGRect) {
    if self.isACircle {
      self.reDrawCircle()
    } else {
      self.reDrawSquare()
    }
    
  }

  func reScalePath(scale scale: CGFloat, panGesture: UIGestureRecognizer) {
    //debugPrint("RESCALE",scale)
    guard self.vwReceiver != nil else {
      return
    }
    
    let position: CGPoint = panGesture.locationInView(self)
    
    //self.vwReceiver!.center = position
    self.vwDummy!.center = position
  
    self.overlaySize.width = scale * self.overlaySize.width
    self.overlaySize.height = scale * self.overlaySize.height
    
    if self.overlaySize.width > self.overlaySizeDefault.width {
      self.overlaySize.width = self.overlaySizeDefault.width
      self.overlaySize.height = self.overlaySizeDefault.height
    }
    
    if self.overlaySize.width < self.overlaySizeMin.width {
      self.overlaySize.width = self.overlaySizeMin.width
      self.overlaySize.height = self.overlaySizeMin.height
    }
    
    let overlayFrame: CGRect = CGRectMake((frame.width - self.overlaySize.width) / 2, (frame.height - self.overlaySize.height)/2, self.overlaySize.width, self.overlaySize.height)
    
    self.reDrawMasking(rect: overlayFrame)
    self.repositionViewScale()
    //debugPrint("RECT POS", self.position, "SIZE", self.overlaySize)
  }
  
  func reMove(panGesture: UIPanGestureRecognizer)  {
    // move it
    guard self.vwReceiver != nil else {
      return
    }
    //let translation = CGSize(width: 10, height: 5)
    //debugPrint("removeCircle")
   
   let position: CGPoint = panGesture.locationInView(self)
   
    switch panGesture.state {
      
    case .Began,.Changed:
       self.vwDummy!.center = position
      
       var rect = self.vwDummy!.frame
       rect.size = (self.overlaySize)
       
       let floatX: CGFloat = self.overlaySize.width + rect.origin.x
       let floatY: CGFloat = self.overlaySize.height + rect.origin.y
       
       if rect.origin.x < 0  {
        rect.origin.x = 0
       }
       
       if rect.origin.y < 0 {
        rect.origin.y = 0
       }
       
       if floatX > self.frame.size.width {
        rect.origin.x = self.frame.size.width - self.overlaySize.width
       }
       
       if floatY > self.frame.size.height {
        rect.origin.y = self.frame.size.height - self.overlaySize.height
       }
       self.vwDummy?.frame = rect
       self.position = CGPointMake(rect.origin.x, rect.origin.y)
       if self.isACircle {
        
        self.pathCircle = UIBezierPath(ovalInRect: rect)
        self.pathCircle.usesEvenOddFillRule = true
        
        let pathRounded: UIBezierPath = UIBezierPath(roundedRect: self.vwReceiver!.bounds, cornerRadius: 0)
        pathRounded.appendPath(self.pathCircle)
        pathRounded.usesEvenOddFillRule = true
        self.shapeCircle.path = pathRounded.CGPath
       
       } else {
        
        self.pathSquare = UIBezierPath(roundedRect:rect, cornerRadius: 0)
        
        self.pathSquare.usesEvenOddFillRule = true
        
        let pathRounded: UIBezierPath = UIBezierPath(roundedRect: self.vwReceiver!.bounds, cornerRadius: 0)
        pathRounded.appendPath(self.pathSquare)
        pathRounded.usesEvenOddFillRule = true
        self.shapeSquare.path = pathRounded.CGPath
       }
       //debugPrint("RECT POS", self.position, "SIZE", self.overlaySize, "point1 ", self.vwScaleTopLeft.frame)
       self.repositionViewScale()
      break
    default:
      break
    }

  }
  
  func reDrawCircle() {
    //debugPrint("RedrawCircle")
    
    let overlayFrame: CGRect = CGRectMake((frame.width - self.overlaySize.width) / 2, (frame.height - self.overlaySize.height)/2, self.overlaySize.width, self.overlaySize.height)
    
    self.position = CGPointMake(overlayFrame.origin.x, overlayFrame.origin.y)
    
    var pathRounded = UIBezierPath(roundedRect: overlayFrame, cornerRadius: 0)
    self.pathCircle = UIBezierPath(ovalInRect: overlayFrame)
    self.pathCircle.usesEvenOddFillRule = true
    
    pathRounded = UIBezierPath(roundedRect: self.bounds, cornerRadius: 0)
    pathRounded.appendPath(self.pathCircle)
    pathRounded.usesEvenOddFillRule = true
  
    self.shapeCircle.path = pathRounded.CGPath
    self.shapeCircle.fillRule = kCAFillRuleEvenOdd
   
  }
  
  func reDrawSquare() {
    
    let overlayFrame: CGRect = CGRectMake((frame.width - self.overlaySize.width) / 2, (frame.height - self.overlaySize.height)/2, self.overlaySize.width, self.overlaySize.height)
    
    self.position = CGPointMake(overlayFrame.origin.x, overlayFrame.origin.y)
   
    var pathRounded = UIBezierPath(roundedRect: overlayFrame, cornerRadius: 0)
    self.pathSquare = UIBezierPath(roundedRect: overlayFrame, cornerRadius: 0)
    self.pathSquare.usesEvenOddFillRule = true
    
    pathRounded = UIBezierPath(roundedRect: self.bounds, cornerRadius: 0)
    pathRounded.appendPath(self.pathSquare)
    pathRounded.usesEvenOddFillRule = true
    
    self.shapeSquare.path = pathRounded.CGPath
    self.shapeSquare.fillRule = kCAFillRuleEvenOdd
    
  }
}

extension UIViewCropArea: DelegateViewDragArea {
  
  func validateEachPoint(vwSender: UIViewDragArea) {
    guard self.vwReceiver != nil else {
      return
    }
    
    let floatMinPointx: CGFloat = (self.vwReceiver?.frame.origin.x)!
    let floatMinPointy: CGFloat = (self.vwReceiver?.frame.origin.y)!
    let floatMaxPointx: CGFloat = (self.vwReceiver?.frame.size.width)!
    let floatMaxPointy: CGFloat = floatMinPointy + (self.vwReceiver?.frame.size.height)!
    
    var pointMin: CGPoint = CGPointZero
    var floatMaxGap: CGFloat = 0.0
    
    if vwSender == self.vwScaleTopRight {
      // #BASED VWSCALEBOTLEFT
      pointMin.x = self.vwScaleBotLeft.center.x + self.overlaySizeMin.width
      pointMin.y = self.vwScaleBotLeft.center.y - self.overlaySizeMin.height
      
      floatMaxGap = self.vwScaleBotLeft.center.y - self.vwScaleTopLeft.center.y
      
      if self.vwScaleTopRight.center.x > floatMaxPointx {
        self.vwScaleTopRight.center.x = floatMaxPointx
        self.vwScaleBotRight.center.x = floatMaxPointx
        
      }
      
      if self.vwScaleTopRight.center.x < pointMin.x {
        self.vwScaleTopRight.center.x = pointMin.x
        self.vwScaleBotRight.center.x = pointMin.x
        
      }
      
      if self.vwScaleTopRight.center.y < floatMinPointy {
        self.vwScaleTopRight.center.y = floatMinPointy
        self.vwScaleTopLeft.center.y = floatMinPointy
        
        self.vwScaleBotRight.center.x = self.vwScaleTopLeft.center.x + floatMaxGap
        self.vwScaleTopRight.center.x = self.vwScaleTopLeft.center.x + floatMaxGap
       
      }
      
      if self.vwScaleTopRight.center.y > pointMin.y {
        self.vwScaleTopRight.center.y = pointMin.y
        self.vwScaleTopLeft.center.y = pointMin.y
       
      }
    
    } else if vwSender == self.vwScaleBotRight {
      // #BASED VWSCALETOPLEFT
      pointMin.x = self.vwScaleTopLeft.center.x + self.overlaySizeMin.width
      pointMin.y = self.vwScaleTopLeft.center.y + self.overlaySizeMin.height
      
      floatMaxGap = self.vwScaleBotLeft.center.y - self.vwScaleTopLeft.center.y
      
      if self.vwScaleBotRight.center.x > floatMaxPointx {
        self.vwScaleTopRight.center.x = floatMaxPointx
        self.vwScaleBotRight.center.x = floatMaxPointx
      }
     
      if self.vwScaleBotRight.center.y > floatMaxPointy {
        self.vwScaleBotRight.center.y = floatMaxPointy
        self.vwScaleBotLeft.center.y = floatMaxPointy
        
        self.vwScaleBotRight.center.x = self.vwScaleTopLeft.center.x + floatMaxGap
        self.vwScaleTopRight.center.x = self.vwScaleTopLeft.center.x + floatMaxGap
      }
      
      if self.vwScaleBotRight.center.x < pointMin.x {
        self.vwScaleBotRight.center.x = pointMin.x
        self.vwScaleTopRight.center.x = pointMin.x
      }
      
      if self.vwScaleBotRight.center.y < pointMin.y {
        self.vwScaleBotRight.center.y = pointMin.y
        self.vwScaleBotLeft.center.y = pointMin.y
      }
    
    } else if vwSender == self.vwScaleBotLeft {
      //#BASED VWSCALETOPRIGHT
      pointMin.x = self.vwScaleTopRight.center.x - self.overlaySizeMin.width
      pointMin.y = self.vwScaleTopRight.center.y + self.overlaySizeMin.height
      
      floatMaxGap = self.vwScaleBotRight.center.y - self.vwScaleTopRight.center.y
      
      if self.vwScaleBotLeft.center.x <= floatMinPointx {
        self.vwScaleBotLeft.center.x = floatMinPointx
        self.vwScaleTopLeft.center.x = floatMinPointx
      }
      
      if self.vwScaleBotLeft.center.y > floatMaxPointy {
        self.vwScaleBotLeft.center.y = floatMaxPointy
        self.vwScaleBotRight.center.y = floatMaxPointy
        
        self.vwScaleBotLeft.center.x = self.vwScaleTopRight.center.x - floatMaxGap
        self.vwScaleTopLeft.center.x = self.vwScaleTopRight.center.x - floatMaxGap
      }
      
      if self.vwScaleBotLeft.center.x > pointMin.x {
        self.vwScaleBotLeft.center.x = pointMin.x
        self.vwScaleTopLeft.center.x = pointMin.x
      }
      
      if self.vwScaleBotLeft.center.y < pointMin.y {
        self.vwScaleBotLeft.center.y = pointMin.y
        self.vwScaleBotRight.center.y = pointMin.y
      }
    
    } else if vwSender == self.vwScaleTopLeft {
      //#BASED VWSCALEBOTRIGHT
      pointMin.x = self.vwScaleBotRight.center.x - self.overlaySizeMin.width
      pointMin.y = self.vwScaleBotRight.center.y - self.overlaySizeMin.height
      
      floatMaxGap = self.vwScaleBotRight.center.y - self.vwScaleTopRight.center.y
      
      if self.vwScaleTopLeft.center.x <= floatMinPointx {
        self.vwScaleTopLeft.center.x = floatMinPointx
        self.vwScaleBotLeft.center.x = floatMinPointx
      }
      
      if self.vwScaleTopLeft.center.y < floatMinPointy {
        self.vwScaleTopLeft.center.y = floatMinPointy
        self.vwScaleTopRight.center.y = floatMinPointy
        
        self.vwScaleTopLeft.center.x = self.vwScaleBotRight.center.x - floatMaxGap
        self.vwScaleBotLeft.center.x = self.vwScaleBotRight.center.x - floatMaxGap
      }

      if self.vwScaleTopLeft.center.x > pointMin.x {
        self.vwScaleTopLeft.center.x = pointMin.x
        self.vwScaleBotLeft.center.x = pointMin.x
      }
      
      if self.vwScaleTopLeft.center.y > pointMin.y {
        self.vwScaleTopLeft.center.y = pointMin.y
        self.vwScaleTopRight.center.y = pointMin.y
      }
    }
  }
  
  func delegateDragViewArea(senderPanGesture: UIPanGestureRecognizer) {
    
    guard self.vwReceiver != nil else {
      return
    }
    
    switch senderPanGesture.state {
      
    case .Began,.Changed:
      let translation = senderPanGesture.translationInView(self)
      
      //debugPrint("translation", translation)
      if let view: UIViewDragArea = senderPanGesture.view as? UIViewDragArea {
        
        var rect: CGRect = CGRectZero
        
        if view == self.vwScaleTopLeft {
          //debugPrint("Top left move done")
          
          var point: CGPoint = CGPointZero
          point.x = view.center.x + translation.y
          point.y = view.center.y + translation.y
          
          view.center = point
          
          point.x = self.vwScaleTopRight.center.x
          point.y = self.vwScaleTopRight.center.y + translation.y
          
          self.vwScaleTopRight.center = point
          
          point.x = self.vwScaleBotLeft.center.x + translation.y
          point.y = self.vwScaleBotLeft.center.y
          
          self.vwScaleBotLeft.center = point
          
          self.validateEachPoint(view)
          
          rect.origin.x = view.center.x
          rect.origin.y = view.center.y - (self.vwReceiver?.frame.origin.y)!
          rect.size.width = self.vwScaleTopRight.center.x - view.center.x
          rect.size.height = self.vwScaleBotLeft.center.y - view.center.y
          
          self.overlaySize = rect.size
          
          self.reDrawMasking(rect: rect)
          
          senderPanGesture.setTranslation(CGPointZero, inView: self.vwReceiver)
          
        } else if view == self.vwScaleTopRight {
          //debugPrint("Top Right Moved DONE")
          
          var point: CGPoint = CGPointZero
          point.x = view.center.x + translation.x
          point.y = view.center.y - translation.x
          
          view.center = point
          
          point.x = self.vwScaleTopLeft.center.x
          point.y = self.vwScaleTopLeft.center.y - translation.x
          
          self.vwScaleTopLeft.center = point
          
          point.x = self.vwScaleBotRight.center.x + translation.x
          point.y = self.vwScaleBotRight.center.y
          
          self.vwScaleBotRight.center = point
          
          self.validateEachPoint(view)
          
          rect.origin.x = self.vwScaleTopLeft.center.x
          rect.origin.y = view.center.y - (self.vwReceiver?.frame.origin.y)!
          rect.size.width = view.center.x - self.vwScaleTopLeft.center.x
          rect.size.height = self.vwScaleBotLeft.center.y - view.center.y
          
          self.overlaySize = rect.size
          
          self.reDrawMasking(rect: rect)
          
          senderPanGesture.setTranslation(CGPointZero, inView: self.vwReceiver)
          
        } else if view == self.vwScaleBotLeft {
          //debugPrint("Bot Left Moved Done validation not yet")
          
          var point: CGPoint = CGPointZero
          point.x = view.center.x - translation.y
          point.y = view.center.y + translation.y
          
          view.center = point
          
          point.x = self.vwScaleTopLeft.center.x - translation.y
          point.y = self.vwScaleTopLeft.center.y
          
          self.vwScaleTopLeft.center = point
          
          point.x = self.vwScaleBotRight.center.x
          point.y = self.vwScaleBotRight.center.y + translation.y
          
          self.vwScaleBotRight.center = point
          
          self.validateEachPoint(view)
          
          rect.origin.x = self.vwScaleTopLeft.center.x
          rect.origin.y = self.vwScaleTopLeft.center.y - (self.vwReceiver?.frame.origin.y)!
          rect.size.width = self.vwScaleTopRight.center.x - view.center.x
          rect.size.height = view.center.y - self.vwScaleTopLeft.center.y
          
          self.overlaySize = rect.size
          
          self.reDrawMasking(rect: rect)

          senderPanGesture.setTranslation(CGPointZero, inView: self.vwReceiver)
          
        } else if view == self.vwScaleBotRight {
          
          var point: CGPoint = CGPointZero
          point.x = view.center.x + translation.x
          point.y = view.center.y + translation.x
          
          view.center = point
          
          point.x = self.vwScaleTopRight.center.x + translation.x
          point.y = self.vwScaleTopRight.center.y
          
          self.vwScaleTopRight.center = point
          
          point.x = self.vwScaleBotLeft.center.x
          point.y = self.vwScaleBotLeft.center.y + translation.x
          
          self.vwScaleBotLeft.center = point
          
          self.validateEachPoint(view)
          
          rect.origin.x = self.vwScaleTopLeft.center.x
          rect.origin.y = self.vwScaleTopLeft.center.y - (self.vwReceiver?.frame.origin.y)!
          rect.size.width = view.center.x - self.vwScaleTopLeft.center.x
          rect.size.height = view.center.y - self.vwScaleTopLeft.center.y 
          
          self.overlaySize = rect.size
          
          self.reDrawMasking(rect: rect)
          
          senderPanGesture.setTranslation(CGPointZero, inView: self.vwReceiver)
        }
      }
      break
    default:
      break
    }
  }
}
