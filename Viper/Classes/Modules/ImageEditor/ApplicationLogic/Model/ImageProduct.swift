//
//  ImageProduct.swift
//  Viper
//
//  Created by Lucy on 7/29/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit
import Kingfisher
import SpriteKit


class ImageProduct: NSObject {

  typealias ImageUrls = (String, AnyObject)
  
  var isAnimate: Bool = true
  var isAnimating: Bool = false
  var isCanCropWhileZoom:Bool = true // depend on the parent view
  var scrollViewParent:UIScrollViewImageZoom?
  
  var vwMasking: UIViewCropArea?
  
  lazy var vwDummy: UIView = UIView()
  
  lazy var imgViewDummy: UIImageView = UIImageView()
  
  private lazy var imgTobeFlipped: UIImage = UIImage()
  
  var imgOriginal: UIImage = UIImage() {
    didSet {
      let imgCG: CGImage = CGImageCreateCopy(self.imgOriginal.CGImage!)!
      self.imgEdited =  UIImage(CGImage: imgCG)
    }
  }
  
  var imgEdited: UIImage?
  var strImageUrl: String = String() {
    didSet {
      
    }
  }
  
  private var floatDegree:CGFloat = 0.0
  
  let context: CIContext = CIContext.cyContextWithOptions(nil)
  
  var filter: CIFilter?
  var floatBrightnessMin: Float = -0.5
  var floatBrightness: Float = 0
  var floatBrightnessDefault: Float = 0
  var floatBrightnessMax: Float = 0.5
  
  var floatContrastMin: Float = 0.5
  var floatContrast: Float = 1.0
  var floatContrastDefault: Float = 1.0
  var floatContrastMax: Float = 1.5
  
  var floatSaturateMin: Float = 0
  var floatSaturate: Float = 1
  var floatSaturateDefault: Float = 1
  var floatSaturateMax: Float = 2
  
  /*Unused for now*/
  var arrStrUrl: [String: AnyObject]?
  var arrImg:[UIImage]?
  var kf_Manager: KingfisherManager?
  /*------------------------------------------*/
  
  
  //MARK: - Refresh Methods
  
  func refreshScrollviewParentImage () {
    
    guard self.scrollViewParent != nil else {
      return
    }

    guard self.scrollViewParent?.imgVw != nil else {
      return
    }
    
    self.scrollViewParent?.displayImage(self.imgEdited!)
  }
  
  //MARK: - Croping Methods
  
  func addMaskingView(vwParent vwParent: UIView, rectImage: CGRect, isSquare: Bool) {
    vwParent.hidden = false
    vwParent.backgroundColor = UIColor.clearColor()
  
    let point: CGPoint = CGPointMake(rectImage.origin.x, rectImage.origin.y)
    let size: CGSize = rectImage.size
    
    self.vwDummy = UIView(frame: rectImage)
    
    self.vwMasking = UIViewCropArea(origin: point, width: size.width, height: size.height)
    self.vwMasking?.vwReceiver = vwDummy
    
    if isSquare {
      
      self.vwMasking?.isACircle = false
      self.vwMasking?.addLayerSquareMask()
      
    } else {
      self.vwMasking?.isACircle = true
      self.vwMasking?.addLayerCircleMask()
    }
    
    vwParent.addSubview(self.vwMasking!)
    
    self.vwMasking?.addScalingView(vwParent: vwParent)
    
  }
  
  
  func cropImage() {
    //debugPrint("CONTENT OFFSET",self.scrollViewParent?.contentOffset)
    
    var newImage: UIImage?
    
    guard self.vwMasking != nil else {
      return
    }
    
    var rectMask: CGRect = CGRectZero
    rectMask.origin = (self.vwMasking?.position)!
    rectMask.size = (self.vwMasking?.overlaySize)!
    
    rectMask.origin.x = (self.scrollViewParent?.contentOffset.x)! + rectMask.origin.x
    rectMask.origin.y = (self.scrollViewParent?.contentOffset.y)! + rectMask.origin.y
    
    let contextImage: UIImage = UIImage(CGImage: self.imgEdited!.CGImage!)
    
    let posX: CGFloat = rectMask.origin.x / (scrollViewParent?.zoomScale)!
    let posY: CGFloat = rectMask.origin.y / (scrollViewParent?.zoomScale)!
    let cgwidth: CGFloat = rectMask.width / (scrollViewParent?.zoomScale)!
    let cgheight: CGFloat = rectMask.height / (scrollViewParent?.zoomScale)!
    
    let rect: CGRect = CGRectMake(posX, posY, cgwidth, cgheight)
    
    // Create bitmap image from context using the rect
    let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextImage.CGImage!, rect)!
    
    // Create a new image based on the imageRef and rotate back to the original orientation
    newImage = UIImage(CGImage: imageRef, scale: self.imgEdited!.scale, orientation: (self.imgEdited?.imageOrientation)!)
    
    //debugPrint("IMAGE NEW",newImage?.size, "ORIGINAL", self.imgOriginal.size)
    self.imgEdited = newImage
    self.refreshScrollviewParentImage()
    self.removeCropView()
    
  }
  
  func removeCropView() {
    guard self.vwMasking != nil else {
      return
    }
    self.vwMasking?.removeScalingView()
    self.vwMasking?.removeFromSuperview()
  }
  
  //MARK: - Adjustment Methods
  
  func removeAllFlipDummy() {
    self.vwDummy.removeFromSuperview()
    self.imgViewDummy.removeFromSuperview()
    
  }
  func adjustBrightness(value:Float, imgVw: UIImageView) {
    self.floatBrightness = value
    let imageCG: CGImage = (self.imgEdited?.CGImage)!
    let imageCI:CIImage = CIImage(CGImage: imageCG)
    
    self.filter  = CIFilter(name: "CIColorControls")!
    self.filter!.setValue(NSNumber(float: value), forKey: kCIInputBrightnessKey)

    self.filter!.setValue(imageCI, forKey: "inputImage")
    
    var outputImage = CIImage()
    outputImage = self.filter!.outputImage!
    
    let cgimg = context.createCGImage(outputImage, fromRect: outputImage.extent)
    let newImage: UIImage = UIImage(CGImage: cgimg!)
    
    imgVw.image = newImage
  }
  
  func adjustContrast(value:Float, imgVw: UIImageView) {
    self.floatContrast = value
    let imageCG: CGImage = (self.imgEdited?.CGImage)!
    let imageCI:CIImage = CIImage(CGImage: imageCG)
    
    self.filter  = CIFilter(name: "CIColorControls")!
    self.filter!.setValue(NSNumber(float: value), forKey: kCIInputContrastKey)
    
    self.filter!.setValue(imageCI, forKey: "inputImage")
    
    var outputImage = CIImage()
    outputImage = self.filter!.outputImage!
    
    let cgimg = context.createCGImage(outputImage, fromRect: outputImage.extent)
    let newImage: UIImage = UIImage(CGImage: cgimg!)
    
    imgVw.image = newImage
  }
  
  func adjustSaturation(value:Float, imgVw: UIImageView) {
    self.floatSaturate = value
    let imageCG: CGImage = (self.imgEdited?.CGImage)!
    let imageCI:CIImage = CIImage(CGImage: imageCG)
    
    self.filter  = CIFilter(name: "CIColorControls")!
    self.filter!.setValue(NSNumber(float: value), forKey: kCIInputSaturationKey)
    
    self.filter!.setValue(imageCI, forKey: "inputImage")
    
    var outputImage = CIImage()
    outputImage = self.filter!.outputImage!
    
    let cgimg = context.createCGImage(outputImage, fromRect: outputImage.extent)
    dispatch_async(dispatch_get_main_queue(),{
      
      let newImage: UIImage = UIImage(CGImage: cgimg!)
      imgVw.image = newImage
      
    })
  }

  //MARK: - Orientation Methods
  func rotateClockwise(isAnimated isAnimated:Bool, vwParent: UIView, rectImage: CGRect?) {
    
    if self.isAnimate == true {
      if rectImage != nil {
        vwParent.clipsToBounds = true
        vwParent.backgroundColor = COLOR_BACKGROUND
        vwParent.hidden = false
        self.imgViewDummy.frame = rectImage!
        self.imgViewDummy.image = self.imgEdited
        self.removeAllFlipDummy()
        //vwParent.backgroundColor = UIColor.redColor()
        vwParent.addSubview(self.imgViewDummy)
        self.animateRotate(90,inviewParent: vwParent)
      }
    }
    
    self.imageRotatedByDegrees(90)
  }
  
  func rotateCounterClockwise(isAnimated isAnimated:Bool, vwParent: UIView, rectImage: CGRect?) {
    
    if self.isAnimate == true {
      
      if rectImage != nil {
        vwParent.clipsToBounds = true
        vwParent.backgroundColor = COLOR_BACKGROUND
        vwParent.hidden = false
        self.imgViewDummy.frame = rectImage!
        self.imgViewDummy.image = self.imgEdited
        self.removeAllFlipDummy()
        //vwParent.backgroundColor = UIColor.redColor()
        vwParent.addSubview(self.imgViewDummy)
        self.animateRotate(-90,inviewParent: vwParent)
      }
    }

    self.imageRotatedByDegrees(-90)
  }
  
  func imageRotatedByDegrees(degrees: CGFloat) {
    
    let degreesToRadians: (CGFloat) -> CGFloat = {
      return $0 / 180.0 * CGFloat(M_PI)
    }
    
    if let size:CGSize = self.imgEdited!.size {
      // calculate the size of the rotated view's containing box for our drawing space
      let rotatedViewBox = UIView(frame: CGRect(origin: CGPointZero, size: size))
      let t = CGAffineTransformMakeRotation(degreesToRadians(degrees))
      rotatedViewBox.transform = t
      let rotatedSize = rotatedViewBox.frame.size
      
      // Create the bitmap context
      UIGraphicsBeginImageContext(rotatedSize)
      let bitmap = UIGraphicsGetCurrentContext()
      
      // Move the origin to the middle of the image so we will rotate and scale around the center.
      CGContextTranslateCTM(bitmap!, rotatedSize.width / 2.0, rotatedSize.height / 2.0);
      
      // Rotate the image context
      CGContextRotateCTM(bitmap!, degreesToRadians(degrees));
      
      // Now, draw the rotated/scaled image into the context
      CGContextScaleCTM(bitmap!, 1.0, -1.0)
      CGContextDrawImage(bitmap!, CGRectMake(-size.width / 2, -size.height / 2, size.width, size.height), (self.imgEdited?.CGImage)!)
      
      let newImage = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      self.imgEdited = newImage
      
      self.refreshScrollviewParentImage()
    }
  }
 
  func animateRotate(degrees: CGFloat, inviewParent vw: UIView) {
   
    let degreesToRadians: (CGFloat) -> CGFloat = {
      return $0 / 180.0 * CGFloat(M_PI)
    }
    
    if let _: UIImageView = self.imgViewDummy {
      self.isAnimating = true
      UIView.animateWithDuration(0.2,
                                 delay: 0.0,
                                 options: UIViewAnimationOptions.CurveEaseInOut,
                                 animations: {
          self.imgViewDummy.transform = CGAffineTransformMakeRotation(degreesToRadians(degrees))
          
        },completion: { finished in
          self.isAnimating = false
          vw.hidden = true
          self.imgViewDummy.removeFromSuperview()
          
          self.imgViewDummy.transform = CGAffineTransformMakeRotation(degreesToRadians(0))
      })
    }
  }
  
  func flipImageX(isAnimated isAnimated:Bool, vwParent: UIView, rectImage: CGRect?){
    if self.isAnimate {
      if rectImage != nil {
        vwParent.clipsToBounds = true
        vwParent.backgroundColor = COLOR_BACKGROUND
        vwParent.hidden = false
        
        self.removeAllFlipDummy()
        
        self.vwDummy.frame = rectImage!
        
        vwParent.addSubview(self.vwDummy)
        
        let rect: CGRect = CGRectMake(0, 0, rectImage!.size.height, rectImage!.size.height)
        self.imgViewDummy.frame = rect
        self.imgViewDummy.image = self.imgEdited
        
        self.imgTobeFlipped = self.getImageFlipped(true, isFlipy: false)
        self.vwDummy.addSubview(self.imgViewDummy)
        
        self.animateFlip(true, vwParent: vwParent)
      }
      
    } else {
      self.imageFlip(true, isFlipy: false)
    }
  }
  
  func flipImageY(isAnimated isAnimated:Bool, vwParent: UIView, rectImage: CGRect?) {
    if self.isAnimate {
      
      if rectImage != nil {
        vwParent.clipsToBounds = true
        vwParent.backgroundColor = COLOR_BACKGROUND
        vwParent.hidden = false
        
        self.removeAllFlipDummy()
        
        self.vwDummy.frame = rectImage!
        vwParent.addSubview(self.vwDummy)
      
        let rect: CGRect = CGRectMake(0, 0, rectImage!.size.height, rectImage!.size.height)
        self.imgViewDummy.frame = rect
        self.imgViewDummy.image = self.imgEdited
        
        self.imgTobeFlipped = self.getImageFlipped(false, isFlipy: true)
        
        self.vwDummy.addSubview(self.imgViewDummy)
        
        vwParent.addSubview(self.vwDummy)
        
        self.animateFlip(false, vwParent: vwParent)
      }
    } else {
      self.imageFlip(false, isFlipy: true)
    }
  }
  
  func imageFlip(isFlipx:Bool, isFlipy: Bool) {
    
   if let size:CGSize = self.imgEdited!.size {
    var yFlip:CGFloat = 1.0
    var xFlip:CGFloat = -1.0
  
    let vw = UIView(frame: CGRect(origin: CGPointZero, size: size))
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(vw.frame.size)
    let bitmap = UIGraphicsGetCurrentContext()
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap!, vw.frame.size.width / 2.0, vw.frame.size.height / 2.0)
    if isFlipx {
      xFlip = 1.0
    }
    
    if isFlipy {
      yFlip = -1.0
    }
    
    CGContextScaleCTM(bitmap!, yFlip, xFlip)
    CGContextDrawImage(bitmap!, CGRectMake(-size.width / 2, -size.height / 2, size.width, size.height), (self.imgEdited?.CGImage)!)
    
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    self.imgEdited = newImage
      
    self.refreshScrollviewParentImage()
    
    }
  }
  
  func getImageFlipped(isFlipx:Bool, isFlipy: Bool) -> UIImage {
    if let size:CGSize = self.imgEdited!.size {
      var yFlip:CGFloat = 1.0
      var xFlip:CGFloat = -1.0
      
      let vw = UIView(frame: CGRect(origin: CGPointZero, size: size))
      
      // Create the bitmap context
      UIGraphicsBeginImageContext(vw.frame.size)
      let bitmap = UIGraphicsGetCurrentContext()
      
      // Move the origin to the middle of the image so we will rotate and scale around the center.
      CGContextTranslateCTM(bitmap!, vw.frame.size.width / 2.0, vw.frame.size.height / 2.0)
      if isFlipx {
        xFlip = 1.0
      }
      
      if isFlipy {
        yFlip = -1.0
      }
      
      CGContextScaleCTM(bitmap!, yFlip, xFlip)
      CGContextDrawImage(bitmap!, CGRectMake(-size.width / 2, -size.height / 2, size.width, size.height), (self.imgEdited?.CGImage)!)
      
      let newImage = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      
      let imgCG: CGImage = CGImageCreateCopy(newImage!.CGImage!)!
      let imgCopy: UIImage = UIImage(CGImage: imgCG)
      
      return imgCopy
    }
  }
  
  func animateFlip(isFLipX: Bool, vwParent: UIView) {
    
    CATransaction.flush()
    
    if let scrollvw: UIScrollViewImageZoom = self.scrollViewParent {
      if let _: UIImageView = scrollvw.imgVw {
        if isFLipX {
          
          self.isAnimating = true
          
          NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(self.animateFlip(_:vwParent:)), object: self.imgViewDummy)
          
          UIView.transitionWithView(self.imgViewDummy, duration: 0.5, options: UIViewAnimationOptions.TransitionFlipFromTop, animations: {
            
            self.imgViewDummy.image = self.imgTobeFlipped
            
          }, completion: { finished in
            
            self.imageFlip(true, isFlipy: false)
            self.removeAllFlipDummy()
            vwParent.hidden = true
            self.isAnimating = false
          })
          
        } else {
          
          self.isAnimating = true
          
          NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(self.animateFlip(_:vwParent:)), object: self.imgViewDummy)
          
          UIView.transitionWithView(self.imgViewDummy, duration: 0.5 , options: .TransitionFlipFromRight, animations: {
            self.imgViewDummy.image = self.imgTobeFlipped
          
          }, completion: { finished in
            self.imageFlip(false, isFlipy: true)
            self.removeAllFlipDummy()
            vwParent.hidden = true
            self.isAnimating = false
          })
          
        }
      }
    }
  }
  
  /*
  func imageRotatedByDegrees(degrees: CGFloat, flip: Bool) -> UIImage {
    
    let radiansToDegrees: (CGFloat) -> CGFloat = {
      return $0 * (180.0 / CGFloat(M_PI))
    }
    let degreesToRadians: (CGFloat) -> CGFloat = {
      return $0 / 180.0 * CGFloat(M_PI)
    }
    
    let size:CGSize = self.scrollViewParent.frame.size
    
    // calculate the size of the rotated view's containing box for our drawing space
    let rotatedViewBox = UIView(frame: CGRect(origin: CGPointZero, size: size))
    let t = CGAffineTransformMakeRotation(degreesToRadians(degrees));
    rotatedViewBox.transform = t
    let rotatedSize = rotatedViewBox.frame.size
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize)
    let bitmap = UIGraphicsGetCurrentContext()
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width / 2.0, rotatedSize.height / 2.0);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, degreesToRadians(degrees));
    
    // Now, draw the rotated/scaled image into the context
    var yFlip: CGFloat
    
    if(flip){
      yFlip = CGFloat(-1.0)
    } else {
      yFlip = CGFloat(1.0)
    }
    
    CGContextScaleCTM(bitmap, yFlip, -1.0)
   // CGContextDrawImage(bitmap, CGRectMake(-size.width / 2, -size.height / 2, size.width, size.height), CGImage)
    
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage
  }*/


  //for download image
  func downloadImageFromUrlArray(strUrl strUrl: String) {
    let group = dispatch_group_create()
    var img: UIImage = UIImage ()
    
    if let stringUrl: String = strUrl {
      if let URL = NSURL(string:stringUrl) {
        dispatch_group_enter(group)
        
        self.kf_Manager = KingfisherManager()
        self.kf_Manager?.downloader.downloadImageWithURL(URL, options: .None , progressBlock: { (receivedSize, totalSize) -> () in
          
          }, completionHandler: { (image, error, imageURL, data) -> () in
            if let imageDownloaded: UIImage = UIImage() {
               img = imageDownloaded
            }
           
            dispatch_group_leave(group)
        })
      }
      
      dispatch_group_notify(group, dispatch_get_main_queue()) { () -> Void in
        if let imgDownloaded: UIImage = img {
          self.imgOriginal = imgDownloaded
        }
      }
    }
  }

  //Incomplete
  func downloadImageFromUrlArray(arrImg arrImgUrls: [ImageUrls]) -> [UIImage] {
    let group = dispatch_group_create()
    var arrImgs: [UIImage] = [UIImage]()
    if let imageURL: [ImageUrls] = arrImgUrls {
      for (_, _) in imageURL.enumerate() {
        if let URL = NSURL(string:"") {
          dispatch_group_enter(group)
          self.kf_Manager?.downloader.downloadImageWithURL(URL, options: .None , progressBlock: { (receivedSize, totalSize) -> () in
            
            }, completionHandler: { (image, error, imageURL, data) -> () in
              if (image != nil){
                arrImgs.append(image!)
              }
              dispatch_group_leave(group)
          })
        }
      }
      
      dispatch_group_notify(group, dispatch_get_main_queue()) { () -> Void in
        if (arrImgs.count>0){
          
        }
      }
    }
    return arrImgs
  }
  
  deinit {
    self.filter = nil
    print("ImageProduct deallocated")
  }
}
