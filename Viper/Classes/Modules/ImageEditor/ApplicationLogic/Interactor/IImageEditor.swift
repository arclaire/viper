//
//  InteractorImageEditor.swift
//  Viper
//
//  Created by Lucy on 7/29/16.
//  Copyright © 2016 cy. All rights reserved.
//

import Foundation
import UIKit

class IImageEditor: NSObject {

  weak var iInputImageEditor: IInputImageEditor?
  weak var iOutputImageEditor: IOutputImageEditor?
  
}


extension IImageEditor: IInputImageEditor {
  
  func doneEditingImage(image: UIImage) {
    self.iOutputImageEditor?.resultEditedImage(image)
  }
  
  func closeImageEditor() {
    
    self.iOutputImageEditor?.dismissImageEditor()
  }
}
