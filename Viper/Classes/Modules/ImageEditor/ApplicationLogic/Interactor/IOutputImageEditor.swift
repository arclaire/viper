//
//  IOutputImageEditor.swift
//  Viper
//
//  Created by Lucy on 7/29/16.
//  Copyright © 2016 cy. All rights reserved.
//

import Foundation
import UIKit

protocol IOutputImageEditor:NSObjectProtocol {
  func resultEditedImage(image: UIImage)
  func dismissImageEditor()
}
