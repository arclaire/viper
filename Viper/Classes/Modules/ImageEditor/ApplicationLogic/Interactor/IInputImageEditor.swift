//
//  IInputImageEditor.swift
//  Viper
//
//  Created by Lucy on 7/29/16.
//  Copyright © 2016 cy. All rights reserved.
//

import Foundation
import UIKit

protocol IInputImageEditor: NSObjectProtocol {
  func closeImageEditor()
  func doneEditingImage(image: UIImage)
}
