//
//  CellItemMenu.swift
//  Viper
//
//  Created by Lucy on 8/3/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

protocol DelegateCellItemMenu: NSObjectProtocol {
  func sliderValueChanged(value : Float)
}
class CellItemMenu: UICollectionViewCell {
  
  //MARK: Outlet
  @IBOutlet weak private var lblTitle: UILabel!
  @IBOutlet weak private var imgIcon: UIImageView!
  
  @IBOutlet weak var slider: UISlider!
  
  //MARK: Attributes
  
  weak var delCellItemMenu: DelegateCellItemMenu?
  
  var dataItemMenu: ItemMenu =  ItemMenu() {
    didSet {
      if let strTitle: String = self.dataItemMenu.strTitle {
        self.lblTitle.text = strTitle
      }
      
      if let strImageName: String = self.dataItemMenu.strImageName {
        self.imgIcon.image = UIImage(named: strImageName)
      }
      
      self.showSlider(dataItemMenu.isSlider)
      
    }
  }
  
 
  
  //MARK: Layouts
  
  //MARK: Init
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.prepareUI()
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.prepareUI()
  }
  
  //MARK: - UI Methods
  
  func prepareUI() {
    self.backgroundColor = COLOR_OPTION_BG
    self.lblTitle.backgroundColor = UIColor.clearColor()
    self.lblTitle.textColor = COLOR_OPTION_TEXT
    self.layer.borderColor = COLOR_BORDER.CGColor
    self.layer.borderWidth = 0.5
    //self.slider.gest
  }
  
  func showSlider(isOnlySlider: Bool) {
    
    if isOnlySlider {
      self.slider.hidden = true
      self.lblTitle.hidden = true
      self.imgIcon.hidden = true
    } else {
      self.slider.hidden = true
      self.lblTitle.hidden = false
      self.imgIcon.hidden = false
    }
  }
  //MARK: - Action Methods
  
  @IBAction func sliderChangedValue(sender: AnyObject) {
    if let slider: UISlider = sender as? UISlider {
      self.delCellItemMenu?.sliderValueChanged(slider.value)
    }
  }
}

