//
//  VCImageEditor.swift
//  Viper
//
//  Created by Lucy on 7/28/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

class VCImageEditor: UIViewController {
  
  //MARK: Outlets
  
  @IBOutlet var vwImageEditor: ViewImageEditor!
  
  //MARK: Attributes
  var eventHandler: MIImageEditor?
  
  var strImgUrl: String?
  var imageSource: UIImage?
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    var className :NSString = NSStringFromClass(VCImageEditor.classForCoder())
    className = className.componentsSeparatedByString(".").last!
    let nib :String = nibNameOrNil ?? className as String
    super.init(nibName: nib, bundle: nibBundleOrNil)
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  //MARK: View Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.vwImageEditor.delViewImageEditor = self
    
    if self.strImgUrl != nil {
      self.vwImageEditor.strImgUrl = self.strImgUrl!
    }
    
    self.vwImageEditor.backgroundColor = UIColor.darkGrayColor()
    
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    if (self.imageSource != nil) {
      self.vwImageEditor.imageSource = self.imageSource
    }
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.vwImageEditor.layoutIfNeeded()
    
    
  }
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    self.vwImageEditor = nil
    
  }
  deinit {
    print("VC Image Editor deallocated")
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func prefersStatusBarHidden() -> Bool {
    return true
  }
}


extension VCImageEditor: DelegateViewImageEditor {
  func doFinishEditingImage(withResultImage img: UIImage?) {
    if img != nil {
      self.eventHandler?.doneEdititng(withImage: img!)
    }
  }
  
  func doCloseImageEditor() {
    self.eventHandler?.closeImageEditor()
  }
}