//
//  ViewImageEditor.swift
//  Viper
//
//  Created by Lucy on 7/28/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit
import Kingfisher

protocol DelegateViewImageEditor: NSObjectProtocol {
  func doCloseImageEditor()
  func doFinishEditingImage(withResultImage img: UIImage?)
  
}

let strActionCrop                                   = "cropMenu"
let strActionCropSquare                             = "cropSquare"
let strActionCropCircle                             = "cropCircle"
let strActionOrientation                            = "orientationMenu"
let strActionOrientationRotateClockwise             = "rotateClockwise"
let strActionOrientationRotateCounterClockwise      = "rotateCounterClockwise"
let strActionOrientationFlipy                       = "flipy"
let strActionOrientationFlipx                       = "flipx"
let strActionFilter                                 = "filterImage"
let strActionAdjustment                             = "adjustImage"
let strActionAdjustSlider                           = "adjustSlider"
let strActionAdjustmentBrightness                   = "adjustImageBrightness"
let strActionAdjustmentContrast                     = "adjustImageContrast"
let strActionAdjustmentSaturation                   = "adjustImageSaturation"

let IMAGE_PLACEHOLDER = UIImage(named: "img_not_found.jpg")

let floatHeightBottomBar: CGFloat = 50
let floatHeightColVw: CGFloat = 88

class ViewImageEditor: UIView {
  
  //MARK: Outlets
  private var vwImageEditor:UIView!
  
  @IBOutlet weak private var scrollView: UIScrollViewImageZoom!
  
  @IBOutlet weak private var colVwButton: UICollectionView!
  
  @IBOutlet weak private var slider: UISliderCustom!
  @IBOutlet weak private var lblFooter: UILabel!
  
  @IBOutlet weak private var vwBottomBar: UIView!
  
  @IBOutlet weak private var vwLineSlider: UIView!
  @IBOutlet weak private var vwParentMask: UIView!
  @IBOutlet weak private var vwContainerSlider: UIView!
  
  @IBOutlet weak private var btnClose: UIButton!
  @IBOutlet weak private var btnDone: UIButton!
  @IBOutlet weak private var flowLayout: UICollectionViewFlowLayout!
  
  //MARK: Attributes Layouts
  @IBOutlet weak private var consVwBottomBarHeight: NSLayoutConstraint!
  @IBOutlet weak private var consColVwHeight: NSLayoutConstraint!
  
  //MARK: Attributes
  
  private let isCanCropWhileZoom: Bool = true
  private var itemMenuActive: ItemMenu? = ItemMenu()
  
  private var arrItemMenus:[ItemMenu] = [ItemMenu]() {
    didSet {
      self.colVwButton.reloadData()
    }
  }
  
  private var arrItemMenusDefault:[ItemMenu] = [ItemMenu]() {
    didSet {
     
    }
  }
  
  private var arrItemMenusOrientation:[ItemMenu] = [ItemMenu]() {
    didSet {
      
    }
  }
  
  private var arrItemMenusCrop:[ItemMenu] = [ItemMenu]() {
    didSet {
      
    }
  }
  
  private var arrItemMenusAdjustment:[ItemMenu] = [ItemMenu]() {
    didSet {
      
    }
  }
  
  private var arrItemMenusAdjustmentSub:[ItemMenu] = [ItemMenu]() {
    didSet {
      
    }
  }
  private var rectImageViewDefault: CGRect = CGRectZero
  
  var imgProduct: ImageProduct? = ImageProduct()
  
  weak var delViewImageEditor: DelegateViewImageEditor?
  
  var strImgUrl: String? {
    didSet {
      self.imgProduct?.strImageUrl = self.strImgUrl!
      self.loadImageFromUrl(strUrl: self.strImgUrl!)
    }
  }
  
  var imageSource: UIImage? {
    didSet {
       self.loadImageFromSource(self.imageSource!)
    }
  }
  //MARK: Initialization
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwImageEditor = self.loadViewFromNIB()
    if let _ = self.vwImageEditor {
      self.addSubview(self.vwImageEditor)
    }
  }
  
  //MARK: Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
    self.prepareUI()
    
    self.prepareItemMenuDefault()
    self.prepareItemMenuOrientation()
    self.prepareItemMenuCrop()
    self.prepareItemAdjustment()
    
    self.switchMenuToArray(arrMenu: self.arrItemMenusDefault, itemMenuParent: nil)
   
    self.backgroundColor = UIColor.whiteColor()
    self.slider.delUISlidercustom = self
   
  }
  
  deinit {
    self.imgProduct = nil
    
    print("View Image Editor deallocated")
  }
  
  //MARK: - UI Methods
  func prepareUI() {
    self.consColVwHeight.constant = floatHeightColVw
    self.consVwBottomBarHeight.constant = floatHeightBottomBar
    
    self.colVwButton.delegate = self
    self.colVwButton.dataSource = self
    
    self.colVwButton.registerNib(UINib(nibName: String(CellItemMenu.classForCoder()), bundle: nil), forCellWithReuseIdentifier: String(CellItemMenu.classForCoder()))
    self.imgProduct?.scrollViewParent = self.scrollView
    self.vwContainerSlider.hidden = true
    self.vwParentMask.hidden = true
  
    self.vwLineSlider.backgroundColor = COLOR_OPTION_TEXT
    self.colVwButton.backgroundColor = COLOR_OPTION_BG
    
    self.vwContainerSlider.backgroundColor = COLOR_OPTION_BG
    self.btnClose.layer.borderWidth = 0.5
    self.btnClose.layer.borderColor = COLOR_BORDER.CGColor
    
//    self.btnDone.titleLabel?.font = Constants.FONT_LATO_BOLD_14
//    self.btnClose.titleLabel?.font = Constants.FONT_LATO_REGULAR_14
    
  }
  
  func prepareAdjustmentUI(strAction strAction:String) {
    
    guard self.imgProduct != nil else {
      return
    }
    self.colVwButton.hidden = true
    self.vwContainerSlider.hidden = false
    
    if strAction == strActionAdjustmentBrightness {
     
      self.slider.minimumValue = (self.imgProduct?.floatBrightnessMin)!
      self.slider.maximumValue = (self.imgProduct?.floatBrightnessMax)!
      self.slider.value = (self.imgProduct?.floatBrightness)!
      self.slider.floatDefaultValue = (self.imgProduct?.floatBrightnessDefault)!
      
      self.itemMenuActive = self.arrItemMenusAdjustment[0]
      
    } else if strAction == strActionAdjustmentContrast {
      
      self.slider.minimumValue = (self.imgProduct?.floatContrastMin)!
      self.slider.maximumValue = (self.imgProduct?.floatContrastMax)!
      self.slider.value = (self.imgProduct?.floatContrast)!
      self.slider.floatDefaultValue = (self.imgProduct?.floatContrastDefault)!
      
      self.itemMenuActive = self.arrItemMenusAdjustment[1]
      
    } else if strAction == strActionAdjustmentSaturation {
      
      self.slider.minimumValue = (self.imgProduct?.floatSaturateMin)!
      self.slider.maximumValue = (self.imgProduct?.floatSaturateMax)!
      self.slider.value = (self.imgProduct?.floatSaturate)!
      self.slider.floatDefaultValue = (self.imgProduct?.floatSaturateDefault)!
      
      self.itemMenuActive = self.arrItemMenusAdjustment[2]
    }
    
     //debugPrint("ACTION", strAction,"VALUE", self.slider.value)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    guard self.scrollView.imgVw != nil else {
      return
    }
    self.rectImageViewDefault = (self.scrollView.imgVw?.frame)!
    //debugPrint("Image", self.scrollView)
    self.slider.prepareMask()
  }
  
  //MARK: - Data Methods
  
  func switchMenuToArray(arrMenu arrMenu:[ItemMenu], itemMenuParent: ItemMenu?) {
    if itemMenuParent != nil {
      self.itemMenuActive = itemMenuParent!
    } else {
      self.itemMenuActive = nil
    }
    
    self.colVwButton.hidden = false
    
    self.arrItemMenus.removeAll()
    self.arrItemMenus = arrMenu
    
    if arrMenu == self.arrItemMenusDefault {
      self.btnClose.setTitle("CANCEL", forState: UIControlState.Normal)
      self.btnDone.setTitle("DONE", forState: UIControlState.Normal)
    } else {
      self.btnClose.setTitle("CANCEL", forState: UIControlState.Normal)
      self.btnDone.setTitle("APPLY", forState: UIControlState.Normal)
    }
  }
  
  func prepareItemMenuDefault() {
    
    let itemMenuOrientation: ItemMenu = ItemMenu()
    itemMenuOrientation.strTitle = "Orientation"
    itemMenuOrientation.strImageName = "icon_orientation"
    itemMenuOrientation.strAction = strActionOrientation
    
    let itemMenuCrop: ItemMenu = ItemMenu()
    itemMenuCrop.strTitle = "Crop"
    itemMenuCrop.strImageName = "icon_crop"
    itemMenuCrop.strAction = strActionCrop
    
    /*let itemMenuFilter: ItemMenu = ItemMenu()
    itemMenuFilter.strTitle = "Filter"
    itemMenuFilter.strImageName = "icon_filter"
    itemMenuFilter.strAction = strActionFilter
    */
    let itemMenuAdjustment: ItemMenu = ItemMenu()
    itemMenuAdjustment.strTitle = "Adjustment"
    itemMenuAdjustment.strImageName = "icon_control"
    itemMenuAdjustment.strAction = strActionAdjustment
    
    self.arrItemMenusDefault = [itemMenuCrop, itemMenuOrientation, itemMenuAdjustment]
  }
  
  func prepareItemMenuOrientation() {
    
    let itemMenuRotateClockwise: ItemMenu = ItemMenu()
    itemMenuRotateClockwise.strTitle = "Rotate 90°"
    itemMenuRotateClockwise.strImageName = "icon_rotateClockwise"
    itemMenuRotateClockwise.strAction = strActionOrientationRotateClockwise
    
    let itemMenuRotateCounterClockwise: ItemMenu = ItemMenu()
    itemMenuRotateCounterClockwise.strTitle = "Rotate -90°"
    itemMenuRotateCounterClockwise.strImageName = "icon_rotateCounterClockwise"
    itemMenuRotateCounterClockwise.strAction = strActionOrientationRotateCounterClockwise
    
    let itemMenuFlipy: ItemMenu = ItemMenu()
    itemMenuFlipy.strTitle = "FlipVertical"
    itemMenuFlipy.strImageName = "icon_flipy"
    itemMenuFlipy.strAction = strActionOrientationFlipy
    
    let itemMenuFlipx: ItemMenu = ItemMenu()
    itemMenuFlipx.strTitle = "FlipHorisontal"
    itemMenuFlipx.strImageName = "icon_flipx"
    itemMenuFlipx.strAction = strActionOrientationFlipx
    
    self.arrItemMenusOrientation = [itemMenuRotateClockwise, itemMenuRotateCounterClockwise, itemMenuFlipy,itemMenuFlipx]
  }
  
  func prepareItemMenuCrop() {
    
    let itemMenuCropSquare: ItemMenu = ItemMenu()
    itemMenuCropSquare.strTitle = "CROP WILL ALWAYS BE 1:1"
    itemMenuCropSquare.strImageName = ""
    itemMenuCropSquare.strAction = strActionCropSquare
    
    self.arrItemMenusCrop = [itemMenuCropSquare]
    
  }
  
  func prepareItemAdjustment() {
    let itemMenuAdjustmentBrightness: ItemMenu = ItemMenu()
    itemMenuAdjustmentBrightness.strTitle = "Brightness"
    itemMenuAdjustmentBrightness.strImageName = "icon_brightness"
    itemMenuAdjustmentBrightness.strAction = strActionAdjustmentBrightness
    
    let itemMenuAdjustmentContrast: ItemMenu = ItemMenu()
    itemMenuAdjustmentContrast.strTitle = "Contrast"
    itemMenuAdjustmentContrast.strImageName = "icon_contrast"
    itemMenuAdjustmentContrast.strAction = strActionAdjustmentContrast
    
    let itemMenuAdjustmentSaturate: ItemMenu = ItemMenu()
    itemMenuAdjustmentSaturate.strTitle = "Saturate"
    itemMenuAdjustmentSaturate.strImageName = "icon_saturate"
    itemMenuAdjustmentSaturate.strAction = strActionAdjustmentSaturation

    let itemMenuAdjustmentSlider: ItemMenu = ItemMenu()
    itemMenuAdjustmentSlider.strTitle = "Adjustment"
    itemMenuAdjustmentSlider.strImageName = "icon_brightness"
    itemMenuAdjustmentSlider.strAction = strActionAdjustSlider
    itemMenuAdjustmentSlider.isSlider = true
    
    self.arrItemMenusAdjustment = [itemMenuAdjustmentBrightness, itemMenuAdjustmentContrast, itemMenuAdjustmentSaturate]
    self.arrItemMenusAdjustmentSub = [itemMenuAdjustmentSlider]
  }
  
  func loadImageFromUrl(strUrl strUrl: String)  {
    if let stringUrl: String = strUrl {//http://www.proify.com/wp-content/uploads/2015/04/landscape-photography-competition.jpg
      if let urlImage: NSURL = NSURL(string: stringUrl) {
        /*self.scrollView.imgVw!.af_setImageWithURL(urlImage, placeholderImage: nil, filter: nil, completion: { response in
        })*/
        
        var rect: CGRect = CGRectZero
        rect.origin.x = 0
        rect.origin.y = 0
        rect.size.width = UIScreen.mainScreen().bounds.width
        rect.size.height = UIScreen.mainScreen().bounds.height - floatHeightBottomBar - floatHeightColVw
        
        let imgVwDummy: UIImageViewWithMask =  UIImageViewWithMask(frame: rect)

        imgVwDummy.kf_setImageWithURL(urlImage, placeholderImage: IMAGE_PLACEHOLDER , optionsInfo: [.Transition(ImageTransition.Fade(0.5))], progressBlock: nil, completionHandler:
          { (image, error, cacheType, imageURL) in
            if error == nil {
              self.scrollView.displayImage(imgVwDummy.image!)
              self.imgProduct?.imgOriginal = imgVwDummy.image!
              //print("SIZE",imgVwDummy.image?.size)
            }
          })
      }
    }
  }
  
  func loadImageFromSource(image: UIImage) {
    self.scrollView.displayImage(image)
    self.imgProduct?.imgOriginal = image

  }
  
  // MARK: - Action Methods
  @IBAction func doTappedButton(sender: AnyObject) {
    if let btnSender: UIButton = sender as? UIButton {
      var strActiveAction: String = ""
      if let itemMenu: ItemMenu = self.itemMenuActive {
        if let strAction = itemMenu.strAction {
          strActiveAction = strAction
        }
      }
      //debugPrint("CURRENT ACTION",strActiveAction)
      switch btnSender {
      case self.btnClose:
        self.actionForButtonClose(strActiveAction)
        break
        
      case self.btnDone:
        self.actionForButtonDone(strActiveAction)
        break
      default:
        break
      }
    }
  }
  
  func actionForButtonClose(strActiveAction: String) {
    
    guard self.imgProduct != nil else {
      return
    }

    self.vwContainerSlider.hidden = true
    if strActiveAction.characters.count > 0 {
      //debugPrint("STRACTION", strActiveAction)
      if strActiveAction == strActionCrop {
        self.imgProduct?.removeCropView()
        self.vwParentMask.hidden = true
        
      }
      if strActiveAction == strActionAdjustmentBrightness || strActiveAction == strActionAdjustmentContrast || strActiveAction == strActionAdjustmentSaturation {
        if strActiveAction == strActionAdjustmentBrightness {
        
          self.imgProduct?.floatBrightness = (self.imgProduct?.floatBrightnessDefault)!
          self.imgProduct?.adjustBrightness((self.imgProduct?.floatBrightnessDefault)!, imgVw: self.scrollView.imgVw!)
        
        } else if strActiveAction == strActionAdjustmentContrast {
          
          self.imgProduct?.floatContrast = (self.imgProduct?.floatContrastDefault)!
          self.imgProduct?.adjustContrast((self.imgProduct?.floatContrastDefault)!, imgVw: self.scrollView.imgVw!)
          
        } else if strActiveAction == strActionAdjustmentSaturation {
          
          self.imgProduct?.floatSaturate = (self.imgProduct?.floatSaturateDefault)!
          self.imgProduct?.adjustSaturation((self.imgProduct?.floatSaturateDefault)!, imgVw: self.scrollView.imgVw!)
        }
        self.switchMenuToArray(arrMenu: self.arrItemMenusAdjustment, itemMenuParent: self.arrItemMenusDefault[2])
      } else {
        self.switchMenuToArray(arrMenu: self.arrItemMenusDefault, itemMenuParent: nil)
      }
      
    } else {
      self.delViewImageEditor?.doCloseImageEditor()
    }
  }
  
  func actionForButtonDone(strActiveAction: String) {
    self.vwContainerSlider.hidden = true
    if strActiveAction.characters.count > 0 {
      //debugPrint("STRACTION", strActiveAction)
      if strActiveAction == strActionCrop {
        self.vwParentMask.hidden = true
        self.imgProduct?.cropImage()
      }
      
      if strActiveAction == strActionAdjustmentBrightness || strActiveAction == strActionAdjustmentContrast || strActiveAction == strActionAdjustmentSaturation {
        self.switchMenuToArray(arrMenu: self.arrItemMenusAdjustment, itemMenuParent:  self.arrItemMenusDefault[2])
      } else {
        self.switchMenuToArray(arrMenu: self.arrItemMenusDefault, itemMenuParent: nil)
      }
      
    } else {
      
      self.imgProduct?.imgEdited = self.scrollView.imgVw?.image
      self.delViewImageEditor?.doFinishEditingImage(withResultImage: self.imgProduct?.imgEdited)
    }
  }
  
  func prepareCropping(isSquare isSquare: Bool) {
    guard self.scrollView.imgVw != nil else {
      return
    }
    
    self.imgProduct?.removeCropView()
    var rect: CGRect = (self.scrollView.imgVw?.frame)!
    //debugPrint("ZOOMSCALE",self.scrollView.zoomScale, self.scrollView.minimumZoomScale)
    if self.scrollView.zoomScale == self.scrollView.minimumZoomScale {
      
    } else {
      if self.isCanCropWhileZoom {
        rect = self.scrollView.frame
        rect.origin.y = 0
      } else {
        self.scrollView.setZoomScale(0, animated: false)
      }
    }
    self.imgProduct?.addMaskingView(vwParent: self.vwParentMask, rectImage: rect, isSquare: isSquare)
  }
  
}


extension ViewImageEditor: DelegateUISliderCustom {
  
  func valueDidChanged(value: Float) {
    var floatValue = self.slider.value / (self.imgProduct?.floatBrightnessMax)! * 100
    if let itemMenu: ItemMenu = self.itemMenuActive {
      if let strAction = itemMenu.strAction {
        
        if strAction == strActionAdjustmentBrightness {
          self.imgProduct?.adjustBrightness(slider.value, imgVw: self.scrollView.imgVw!)
          floatValue = self.slider.value / (self.imgProduct?.floatBrightnessMax)! * 100
          
        } else if strAction == strActionAdjustmentContrast {
          self.imgProduct?.adjustContrast(slider.value, imgVw: self.scrollView.imgVw!)
          
          //SATURATION CONVERT
          if self.slider.value > 1 {
            floatValue = (self.slider.value * 100) - 100
          }else if self.slider.value < 1 {
            floatValue = -(self.slider.value * 100)
          }else {
            floatValue = 0
          }
          
        } else if strAction == strActionAdjustmentSaturation {
          self.imgProduct?.adjustSaturation(slider.value, imgVw: self.scrollView.imgVw!)
          //SATURATION CONVERT
          if self.slider.value > 1 {
            floatValue = (self.slider.value * 100) - 100
          }else if self.slider.value < 1 {
            floatValue = -(self.slider.value * 100)
          }else {
            floatValue = 0
          }
        }
        
        self.slider.lblIndicator.text = String(format:"%d",Int(floatValue))
      }
    }
  }
}

extension ViewImageEditor: UICollectionViewDataSource {
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if self.arrItemMenus.count > 0 {
      return self.arrItemMenus.count
    }
      return 1
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell : CellItemMenu = collectionView.dequeueReusableCellWithReuseIdentifier(String(CellItemMenu.classForCoder()), forIndexPath: indexPath) as! CellItemMenu
    
    if let dataMenu: ItemMenu = self.arrItemMenus[indexPath.row] {
      cell.dataItemMenu = dataMenu
    }
    
    return cell
  }
}

extension ViewImageEditor: UICollectionViewDelegate {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 1
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    
    let sectionInset = UIEdgeInsets(top: 1, left: 1.5, bottom: 1, right: 1)
    
    return sectionInset
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    
    if self.arrItemMenus.count == 1 {
      return CGSize(width: self.colVwButton.frame.width , height: self.colVwButton.frame.height)
    } else if self.arrItemMenus.count < 4 {
      let floatTotalItem: CGFloat = CGFloat(self.arrItemMenus.count)
      return CGSize(width: (self.colVwButton.frame.width/floatTotalItem)-2 , height: self.colVwButton.frame.height-2)
    } else {
      return CGSize(width: floatHeightColVw-2 , height: self.colVwButton.frame.height-2)
    }
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    if let cell: CellItemMenu = collectionView.cellForItemAtIndexPath(indexPath) as? CellItemMenu {
      
      guard self.scrollView.imgVw != nil else {
        return
      }
      
      guard self.imgProduct != nil else {
        return
      }
      
      if let dataItemMenu: ItemMenu = cell.dataItemMenu {
        if let strAction: String = dataItemMenu.strAction {
          //debugPrint("CURRENT ACTION", strAction)
          switch strAction {
          case strActionCrop:
            self.switchMenuToArray(arrMenu: self.arrItemMenusCrop, itemMenuParent: dataItemMenu)
            self.prepareCropping(isSquare: true)
            break
          case strActionCropCircle:
            //self.prepareCropping(isSquare: false)
            break
          case strActionCropSquare:
            //self.prepareCropping(isSquare: true)
            break
          case strActionFilter:
            break
          case strActionAdjustment:
            self.switchMenuToArray(arrMenu: self.arrItemMenusAdjustment, itemMenuParent: dataItemMenu)
            break
          case strActionAdjustmentBrightness, strActionAdjustmentContrast, strActionAdjustmentSaturation:
            self.prepareAdjustmentUI(strAction: strAction)
            break
          case strActionOrientation :
            self.switchMenuToArray(arrMenu: self.arrItemMenusOrientation, itemMenuParent: dataItemMenu)
          case strActionOrientationRotateClockwise:
            if !(self.imgProduct?.isAnimating)! {
              self.imgProduct?.rotateClockwise(isAnimated: true, vwParent: self.vwParentMask, rectImage: (self.scrollView.imgVw?.frame)!)
            }
            break
          case strActionOrientationRotateCounterClockwise:
            if !(self.imgProduct?.isAnimating)! {
               self.imgProduct?.rotateCounterClockwise(isAnimated: true, vwParent: self.vwParentMask, rectImage: (self.scrollView.imgVw?.frame)!)
            }
           
            break
          case strActionOrientationFlipx:
            if !(self.imgProduct?.isAnimating)! {
              self.imgProduct?.flipImageX(isAnimated: true, vwParent: self.vwParentMask, rectImage: (self.scrollView.imgVw?.frame)!)
            }
            
            break
          case strActionOrientationFlipy:
            if !(self.imgProduct?.isAnimating)! {
              self.imgProduct?.flipImageY(isAnimated: true, vwParent: self.vwParentMask, rectImage: (self.scrollView.imgVw?.frame)!)
            }
            break
          default:
            break
          }
        }
      }
    }
  }
  
}
