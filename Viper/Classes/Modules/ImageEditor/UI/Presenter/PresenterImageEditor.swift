//
//  PresenterImageEditor.swift
//  Viper
//
//  Created by Lucy on 7/29/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

class PresenterImageEditor: NSObject {

  var wfImageEditor: WfImageEditor?
  var iImageEditor: IImageEditor?
  var iInputImageEditor: IInputImageEditor?
  
  
}

extension PresenterImageEditor: MIImageEditor {
  
  func doneEdititng(withImage img: UIImage) {
    
    self.iInputImageEditor?.doneEditingImage(img)
  }
  
  func closeImageEditor() {
    self.iInputImageEditor?.closeImageEditor()
  }
}

extension PresenterImageEditor: IOutputImageEditor {
  
  func dismissImageEditor() {
    self.wfImageEditor?.dismissVcImageEditor()
  }
  
  func resultEditedImage(image: UIImage) {
    self.wfImageEditor?.dismissVCImageEditorWithImage(image)
  }
}
