//
//  WfImageEditor.swift
//  Viper
//
//  Created by Lucy on 7/28/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit
let idVcImageEditor = "VCImageEditor"

protocol DelegateWFImageEditor: NSObjectProtocol {
  func doneEditingImage(image: UIImage)
}

class WfImageEditor: NSObject {

  var vcImageEditor : VCImageEditor?
  var presImageEditor: PresenterImageEditor!
  
  var strImgUrl: String?
  var imgSource: UIImage?
  
  weak var delWFImageEditor: DelegateWFImageEditor?
  
  func pushVCImageEditorFromVc(vc vc:UIViewController) {
    self.vcImageEditor = VCImageEditor() //self.vcImageEditorFromStoryboard()
    vc.navigationController?.pushViewController(self.vcImageEditor!, animated: true)
  }
  
  func presentVCImageEditorFromVC(vc vc:UIViewController) {
    self.vcImageEditor =  VCImageEditor() //self.vcImageEditorFromStoryboard()
    self.configureImageEditorDepedency()
    
    vc.navigationController?.presentViewController(self.vcImageEditor!, animated: true, completion: nil)
  }
  
  func presentVCImageEditorFromVCNotNavigation(vc vc:UIViewController) {
    self.vcImageEditor =  VCImageEditor() //self.vcImageEditorFromStoryboard()
    self.configureImageEditorDepedency()
    
    vc.presentViewController(self.vcImageEditor!, animated: true, completion: nil)
  }
  func dismissVcImageEditor() {
    self.vcImageEditor?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func dismissVCImageEditorWithImage(image: UIImage) {
    
    self.delWFImageEditor?.doneEditingImage(image)
  }
  
  func configureImageEditorDepedency() {
    let iImageEditor: IImageEditor = IImageEditor()
    self.presImageEditor = PresenterImageEditor()
    self.presImageEditor.wfImageEditor = self
    self.presImageEditor.iImageEditor = iImageEditor
    self.presImageEditor.iImageEditor?.iOutputImageEditor = self.presImageEditor
    self.presImageEditor.iInputImageEditor = iImageEditor
    
    self.vcImageEditor?.eventHandler = self.presImageEditor
    
    if self.strImgUrl != nil {
      self.vcImageEditor?.strImgUrl = self.strImgUrl!
    }
    
    if (self.imgSource != nil) {
     self.vcImageEditor?.imageSource = self.imgSource!
    }
  }
  
  func vcImageEditorFromStoryboard() -> VCImageEditor {
    let storyboard = mainStoryboard()
    let viewController = storyboard.instantiateViewControllerWithIdentifier(idVcImageEditor) as! VCImageEditor
    return viewController
  }
  
  func mainStoryboard() -> UIStoryboard {
    let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
    return storyboard
  }

  
}
