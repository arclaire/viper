//
//  InterfaceLogin.swift
//  Viper
//
//  Created by Lucy on 7/4/16.
//  Copyright © 2016 cy. All rights reserved.
//

import Foundation

protocol MILogin: NSObjectProtocol {
  
  func didTappedButtonLogin()
  
  func didTappedShortCut()
}