//
//  PresenterLogin.swift
//  Viper
//
//  Created by Lucy on 7/11/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

class PresenterLogin: NSObject {

  var wfLogin: WfLogin?
  var iLogin: ILogin?
  var iInputLogin: IInputLogin?
}


extension PresenterLogin: MILogin {
  
  func didTappedButtonLogin() {
    
    self.iInputLogin!.processLoginFacebook()
  }
  
  func didTappedShortCut() {
    self.iInputLogin?.processShortCut()
  }
}

extension PresenterLogin: IOutputLogin {
  func outputLoginError() {
    
  }
  
  func outputLoginSuccess() {
    self.wfLogin?.pushVCHome()
  }
  
  func outputLoginFailed() {
    
  }
}