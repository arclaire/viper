//
//  wfLogin.swift
//  Viper
//
//  Created by Lucy on 6/20/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

let idVcLogin = "VCLogin"
class WfLogin: NSObject {

  var wfRoot: WfRoot?
  var wfHome: WfHome?
  
  var vcLogin: VCLogin?
  var presLogin: PresenterLogin?
  
  override init() {
    super.init()
    
  }
  
  func anchorAsRoot(window: UIWindow) {
    let viewController: VCLogin = self.vcLoginFromStoryboard()
   
    self.vcLogin = viewController
    self.vcLogin?.eventHandler = self.presLogin
    
    self.wfRoot?.showRootViewController(self.vcLogin!, inWindow: window)
  }
  
  func pushVCHome() {
    self.wfHome?.pushHomeVCFromVc(vc: self.vcLogin!)
  }
  
  func vcLoginFromStoryboard() -> VCLogin {
    let storyboard = mainStoryboard()
    let viewController = storyboard.instantiateViewControllerWithIdentifier(idVcLogin) as! VCLogin
   
    return viewController
  }
  
  func mainStoryboard() -> UIStoryboard {
    let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
    return storyboard
  }
  
  
}
