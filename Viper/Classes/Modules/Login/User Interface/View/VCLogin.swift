//
//  LoginVc.swift
//  Viper
//
//  Created by Lucy on 6/19/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

class VCLogin: UIViewController {

  //MARK: Outlets
  
  @IBOutlet var vwLogin: ViewLogin!
  
  var window: UIWindow?
  //MARK: Attributes
  
   var eventHandler:MILogin?
  
  //MARK: View Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.vwLogin.delViewLogin = self
    
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    
  }
  
}

extension VCLogin: DelegateViewLogin {
  func doTappedButtonLogin() {
    self.eventHandler?.didTappedButtonLogin()
    /* create a window
    window = UIWindow(frame: self.view.frame)
     let vw: UIView = UIView(frame: self.view.frame)
    vw.backgroundColor = UIColor.blackColor()
     let btn: UIButton = UIButton(frame:CGRectMake(0,0,100,100))
     btn.backgroundColor = UIColor.redColor()
     vw.addSubview(btn)
     btn.addTarget(self, action: #selector(self.sample), forControlEvents: UIControlEvents.TouchUpInside)
    window?.windowLevel = UIWindowLevelAlert
    window!.addSubview(vw)
    window!.makeKeyAndVisible()
    */
    //let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
     //appDelegate.window!.addSubview(vw)
  }
  
  func delTappedShorCut() {
    self.eventHandler?.didTappedShortCut()
  }
  func sample(){
    print("sample")
  }
}