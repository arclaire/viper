//
//  LoginView.swift
//  Viper
//
//  Created by Lucy on 6/20/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

protocol DelegateViewLogin: NSObjectProtocol {
  func doTappedButtonLogin()
  func delTappedShorCut()
}

class ViewLogin: UIView {

  private var vwLogin:UIView!
  
  //MARK: Outlets
  
  @IBOutlet private weak var btnLogin: UIButton!
  
  @IBOutlet weak var btnShortCut: UIButton!
  //MARK: Attributes
  weak var delViewLogin: DelegateViewLogin?

  //MARK: Initialization
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwLogin = self.loadViewFromNIB()
    if let _ = self.vwLogin {
      self.addSubview(self.vwLogin)
    }
  }
  
  //MARK: Lifecycle
  override func awakeFromNib() {

  }

  //MARK: Action Methods
  @IBAction func doTapButton(sender: AnyObject) {
    
    //print("Tapped button")
    if let btnSender: UIButton = sender as? UIButton {
      if btnSender == self.btnLogin {
        self.delViewLogin?.doTappedButtonLogin()

      } else if btnSender == self.btnShortCut {
        self.delViewLogin?.delTappedShorCut()
      }
    }
  }
}
