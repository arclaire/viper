//
//  IInputLogin.swift
//  Viper
//
//  Created by Lucy on 7/27/16.
//  Copyright © 2016 cy. All rights reserved.
//

import Foundation
protocol IInputLogin: NSObjectProtocol {
  func processLoginFacebook()
  func processShortCut()
}