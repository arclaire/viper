//
//  LoginInteractor.swift
//  Viper
//
//  Created by Lucy on 6/20/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

import FBSDKCoreKit
import FBSDKLoginKit

class ILogin: NSObject {
  
  weak var iOutputLogin: IOutputLogin?
  weak var iInputLogin: IInputLogin?
  
}

extension ILogin: IInputLogin {
  
  func processShortCut() {
    var dataUser: User = User()
    dataUser.storeData("132291397210584", strName: "adsasdas", strImageProfile:"http://id.animepo.com/wp-content/uploads/sites/3/2016/04/1460367241_284_Audi-R8-tampil-di-Final-Fantasy-XV-Kingsglaive-dengan-sentuhan-detail-yang-menawan.jpg")
    
    self.iOutputLogin?.outputLoginSuccess()
    
  }
  
  func processLoginFacebook() {
    let loginFb: FBSDKLoginManager = FBSDKLoginManager()
    loginFb.loginBehavior = .Native // .Web workaround when default native can't
    loginFb.logOut()
    
    print("SDK version \(FBSDKSettings .sdkVersion())")
    let strFBPermission = ["email","user_location","public_profile","public_profile"]
    
    loginFb.logInWithReadPermissions(strFBPermission, fromViewController: nil, handler: { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
      if error != nil {
        loginFb.logOut()
        
        let strError:String = String(error.localizedDescription)
        UIAlertView(title: "Error".uppercaseString, message: strError, delegate: nil, cancelButtonTitle: "Ok").show()
        
      } else if result.isCancelled {
        loginFb.logOut()
        
      } else {
        
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"name, email,first_name,location,last_name,locale,hometown"])
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
          
          if error != nil  {
            
            loginFb.logOut()
            
            let strError:String = String(error.localizedDescription)
            UIAlertView(title: "Error".uppercaseString, message: strError, delegate: nil, cancelButtonTitle: "Ok").show()
            
          } else {
            
            if let dictionaryResult:[String: AnyObject] = result as? [String: AnyObject] {
                            
              if let strId: String = dictionaryResult["id"] as? String {
                if let strName: String = dictionaryResult["name"] as? String {
                  var dataUser: User = User()
                  dataUser.storeData(strId, strName: strName, strImageProfile:nil)
                }
              }
              
              self.iOutputLogin?.outputLoginSuccess()
              debugPrint("dataResult",dictionaryResult)
            } else {
              
              loginFb.logOut()
            }
          }
        })
      }
    })
  }
}
