//
//  PresenterHome.swift
//  Viper
//
//  Created by Lucy on 7/28/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

class PresenterHome: NSObject {
  var wfHome: WfHome?
  var iHome: IHome?
  var iInputHome: IInputHome?
}


extension PresenterHome: MIHome {
  
  func didTappedButtonImageEditor() {
    self.iInputHome?.doGotoImageEditor()
  }
  
  func didTappedButtonCamera() {
    
  }
}

extension PresenterHome: IOutputHome {
  func goToImageEditor() {
    self.wfHome?.presentVCImageEditor()
  }
}