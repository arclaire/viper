//
//  ViewHome.swift
//  Viper
//
//  Created by Lucy on 7/4/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

protocol DelegateViewHome: NSObjectProtocol {
  func doGoToImageEditor()
  
}

class ViewHome: UIView {

  private var vwHome:UIView!
  
  @IBOutlet weak var btnImageEditor: UIButton!
  
  //MARK: Outlets
  
 
  //MARK: Attributes
  weak var delHomeView: DelegateViewHome?
  
  //MARK: Initialization
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    
    self.vwHome = self.loadViewFromNIB()
    if let _ = self.vwHome {
      self.addSubview(self.vwHome)
    }
  }
  
  //MARK: Lifecycle
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  //MARK: Data Methods
 
  
  //MARK: ButtonAction
  
  @IBAction func doTapButton(sender: AnyObject) {
    
    if let btnSender: UIButton = sender as? UIButton {
      
      if btnSender == self.btnImageEditor {
        self.delHomeView?.doGoToImageEditor()
      }
    }
  }
  
}
