//
//  VCHome.swift
//  Viper
//
//  Created by Lucy on 7/4/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

class VCHome: UIViewController {

  
  //MARK: Outlets
  
  @IBOutlet var vwHome: ViewHome!
  //MARK: Attributes
  
  var eventHandler: MIHome?
  
  //MARK: View Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.vwHome.delHomeView = self
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    
  }
  
}

extension VCHome: DelegateViewHome {
  
  func doGoToImageEditor() {
    
    self.eventHandler?.didTappedButtonImageEditor()
  }
}