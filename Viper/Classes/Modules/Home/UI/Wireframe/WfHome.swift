//
//  WfHome.swift
//  Viper
//
//  Created by Lucy on 7/4/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit
let idVcHome = "VCHome"
class WfHome: NSObject {
  
  var wfRoot : WfRoot?
  var wfImageEditor: WfImageEditor?
  var vcHome : VCHome?
  var presHome: PresenterHome?
  
  func anchorAsRoot(window: UIWindow) {
    let viewController = self.vcHomeFromStoryboard()
    //viewController.eventHandler = listPresenter
    self.vcHome = viewController
    // listPresenter!.userInterface = viewController
    self.wfRoot?.showRootViewController(viewController, inWindow: window)
  }
  
  func pushHomeVCFromVc(vc vc:UIViewController) {
    self.vcHome = self.vcHomeFromStoryboard()
    self.vcHome?.eventHandler = presHome
    vc.navigationController?.pushViewController(self.vcHome!, animated: true)
  }
  
  func vcHomeFromStoryboard() -> VCHome {
    let storyboard = mainStoryboard()
    let viewController = storyboard.instantiateViewControllerWithIdentifier(idVcHome) as! VCHome
    return viewController
  }
  
  
  func mainStoryboard() -> UIStoryboard {
    let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
    return storyboard
  }

  func presentVCImageEditor() {
    let user: User = User()
    
    if let strImgUrl: String = user.getUserProfilePictureUrlFromUserDefaults() {
      self.wfImageEditor?.strImgUrl = strImgUrl
      print("WFHome get UrL", strImgUrl)
    }
  
    
    self.wfImageEditor?.presentVCImageEditorFromVC(vc: self.vcHome!)
  }
}
