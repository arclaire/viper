//
//  MIHome.swift
//  Viper
//
//  Created by Lucy on 7/28/16.
//  Copyright © 2016 cy. All rights reserved.
//

import Foundation

protocol MIHome: NSObjectProtocol {
  
  func didTappedButtonCamera()
  func didTappedButtonImageEditor()
}
