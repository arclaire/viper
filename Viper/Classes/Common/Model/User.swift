//
//  User.swift
//  Viper
//
//  Created by Lucy on 6/20/16.
//  Copyright © 2016 cy. All rights reserved.
//

import Foundation
import FBSDKLoginKit

struct User {
  var strId : String?
  var strName : String?
  var strImageProfile: String?
  
  mutating func storeData(strId: String, strName: String, strImageProfile: String?) {
  
    self.strId = strId
    self.strName = strName
   
    
    let user = NSUserDefaults.standardUserDefaults() // temporary nsuserdefaults
    if self.strImageProfile != nil {
      self.strImageProfile = strImageProfile
      user.setObject(self.strImageProfile, forKey: "urlProfileImage")
    } else {
      if let strID:String = strId {
        if !strID.isEmpty{
          let strURlFbBased: String = "https://graph.facebook.com/"
          let strPostFixSize: String = "picture?width=640&height=640"
          self.strImageProfile = String(format:"%@%@/%@",strURlFbBased,strID,strPostFixSize)
          user.setObject(self.strImageProfile, forKey: "urlProfileImage")
        }
      }
    }
    
    user.setObject(self.strId, forKey: "userid")
    user.setObject(self.strName, forKey: "name")
  }
  
  func getUserProfilePictureUrlFromUserDefaults() -> String? {
    let user = NSUserDefaults.standardUserDefaults()
    var strUrl: String?
    if let strImageUrl: String = user.stringForKey("urlProfileImage") {
      strUrl = strImageUrl
    }
    
    return strUrl
  }
  
  func getFBProfilePictureURL() -> NSURL {
    let strUrlDummy: String = "https://pbs.twimg.com/profile_images/600060188872155136/st4Sp6Aw.jpg"
    
    var strUrlProfilePicture: String = String(format: strUrlDummy)
    
    if let strID:String = self.strId {
      if !strID.isEmpty {
        let strURlFbBased: String = "https://graph.facebook.com/"
        let strPostFixSize: String = "picture?width=640&height=640"
        strUrlProfilePicture = String(format: strURlFbBased,strID,strPostFixSize)
      }
    }
    
    let urlProfilePicture: NSURL = NSURL(string: strUrlProfilePicture)!
    return urlProfilePicture
  }
  
  func getFBProfilePictureWithGraph() {
    if FBSDKAccessToken.currentAccessToken() != nil {
      let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
      graphRequest.startWithCompletionHandler({
        (connection, result, error) -> Void in
        if ((error) != nil)
        {
          print("Error: \(error)")
        }
        else if error == nil
        {
          let facebookID: NSString = (result.valueForKey("id")
            as? NSString)!
          
          let pictureURL = "https://graph.facebook.com/\(facebookID)/picture?type=large&return_ssl_resources=1"
          
          //self.profileNameLbl.text = (result.valueForKey("name")             as? String)!
          
          let URLRequest = NSURL(string: pictureURL)
          let URLRequestNeeded = NSURLRequest(URL: URLRequest!)
          
          NSURLConnection.sendAsynchronousRequest(URLRequestNeeded, queue: NSOperationQueue.mainQueue(), completionHandler: {(response: NSURLResponse?,data: NSData?, error: NSError?) -> Void in
            
            if error == nil {
              //let picture = UIImage(data: data!)
             // self.profileImageView.image = picture
            }
            else {
              print("Error: \(error!.localizedDescription)")
            }
          })
        }
      })
    }
  }
}
