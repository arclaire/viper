//
//  wfRoot.swift
//  Viper
//
//  Created by Lucy on 6/20/16.
//  Copyright © 2016 cy. All rights reserved.
//

import UIKit

class WfRoot: NSObject {

  func showRootViewController(viewController: UIViewController, inWindow: UIWindow) {
    let navigationController = self.navigationControllerFromWindow(inWindow)
    navigationController.viewControllers = [viewController]
  }
  
  func navigationControllerFromWindow(window: UIWindow) -> UINavigationController {
    let navigationController = window.rootViewController as! UINavigationController
    return navigationController
  }
  
 
}
