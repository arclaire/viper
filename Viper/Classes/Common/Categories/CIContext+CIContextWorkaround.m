//
//  CIContext+CIContextWorkaround.m
//  Viper
//
//  Created by Lucy on 10/6/16.
//  Copyright © 2016 cy. All rights reserved.
//

#import "CIContext+CIContextWorkaround.h"

@implementation CIContext (CIContextWorkaround)

+ (CIContext *)cyContextWithOptions:(NSDictionary<NSString *, id> *)options {
    return [CIContext contextWithOptions:options];
}

@end
