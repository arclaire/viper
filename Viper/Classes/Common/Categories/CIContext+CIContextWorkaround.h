//
//  CIContext+CIContextWorkaround.h
//  Viper
//
//  Created by Lucy on 10/6/16.
//  Copyright © 2016 cy. All rights reserved.
//

#import <CoreImage/CoreImage.h>

@interface CIContext (CIContextWorkaround)

+ (CIContext *)cyContextWithOptions:(NSDictionary<NSString *, id> *)options;

@end
