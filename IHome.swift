//
//  IHome.swift
//  Viper
//
//  Created by Lucy on 7/29/16.
//  Copyright © 2016 cy. All rights reserved.
//

import Foundation

class IHome: NSObject {
  
  weak var iInputHome: IInputHome?
  weak var iOuputHome: IOutputHome?
  
}

extension IHome: IInputHome {
 
  func doGotoImageEditor() {
    self.iOuputHome?.goToImageEditor()
  }
}